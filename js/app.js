// Foundation JavaScript
// Documentation can be found at: http://foundation.zurb.com/docs
$(document).foundation();
window.vBook =
{
  soundEffectsManager: null,
  positionPoint: null,

  shortPauseTime: 100,
  pauseTime: 400,
  longPauseTime: 800,
  positionPointScrollTime: 200,
  screenScrollTime: 500,
  scooterAnimationTime: 2000,
  browsableWaitTime: 2000,
  scrollWaitingPeriod: 1000,

  figurePauseTime: 2000,

  moveBrowsableAnimationGoBackDuration: 200,
  moveBrowsableAnimationShowDuration: 300,

  shouldPlaySkipSound: false,

  listingRegEx: /listing(\s)+[0-9]+\.[0-9]+/gi,
  figureRegEx: /figure(\s)+[0-9]+\.[0-9]+/gi,

  browsableRegExes:
  [
    /figure(\s)+[0-9]+\.[0-9]+/gi,
    /listing(\s)+[0-9]+\.[0-9]+/gi,
    /table(\s)+[0-9]+\.[0-9]+/gi
  ],

  logMessages: true,
  log: function(message)
  {
    if(!this.logMessages){ return; }

    console.log(message);
  },
  triggerEvent: function(eventName, eventArg)
  {
    setTimeout(function()
    {
      $(document).trigger(eventName, eventArg);
    }, 10);
  },

  getTopNavBarHeight: function()
  {
    return $("#top-bar").height();
  },

  utils:
  {
    getBrowsableShowMode: function (textFragment, browsable)
    {
      var browsablesShowModeFitScreen = "browsablesShowModeFitScreen";
      var browsablesShowModeSticky = "browsablesShowModeSticky";

      return window.vBook.utils.canBothElementsFitIntoScreen(textFragment, browsable.heading || browsable)
           ? browsablesShowModeFitScreen
           : browsablesShowModeSticky;
    },

    getQuestionContainers: function()
    {
      return this.getBookMarkupChildren(".questionsGroupContainer");
    },

    getBookMarkupChildren: function(childSelector)
    {
      var $bookMarkupContainer = $("#bookMarkupContainer");
      var $textItemContainers = $bookMarkupContainer.children(childSelector);

      return $textItemContainers.toArray();
    },

    hasBrowsables: function(element)
    {
      if(element == null){ return false; }

      return element.browsables != null
          && element.browsables.length > 0;
    },

    getMostImportantBrowsable: function (browsables)
    {
      if(browsables == null || browsables.length == null) { return null; }
      if(browsables.length == 1) { return browsables[0]; }

      var listingAsImage, listingAsCode, figure, table;

      for(var i = 0; i < browsables.length; i++)
      {
        var browsable = browsables[i];

        switch(browsable.nodeName.toLowerCase())
        {
          case "img":
            if(browsable.heading != null
            && browsable.heading.textContent.toLowerCase().match(window.vBook.figureRegEx))
            {
              figure = browsable;
            }
            else
            {
              listingAsImage = browsable;
              return listingAsImage; //The cueball is first and should more most important.
            }
            break;
          case "form":
            listingAsCode = browsable;
            break;
          case "table":
            table = browsable;
            break;
          default:
            break;
        }
      }

      if(listingAsImage) { return listingAsImage; }
      if(listingAsCode) { return listingAsCode; }
      if(figure){ return figure; }
      if(table){ return table; }

      return null;
    },

    getMatchingBrowsable: function(browsables, text)
    {
      if(browsables == null || browsables.length == null) { return null; }

      for(var i = 0; i < browsables.length; i++)
      {
        var browsable = browsables[i];

        var headingText = $(browsable.heading).text();

        if(headingText.indexOf(text) != -1) { return browsable; }
      }

      return browsable;
    },

    getNextCueForPlaying: function(element)
    {
      var startOver = false;
      var $element = $(element);
      if($element.is(".text-fragment-played"))
      {
        startOver = true;
      }

      if(!startOver && element.lastPlayedCue) { return element.lastPlayedCue; }
      if(element.cues != null) { return element.cues[0]; }

      return null;
    },

    scrollToPosition: function(position, callback)
    {
      $('html,body').velocity
      (
        "scroll",
        {
          duration: window.vBook.screenScrollTime,
          offset: position,
        }
      );

      //velocity complete not triggered, so do it manually
      if(callback)
      {
        setTimeout(callback, window.vBook.screenScrollTime);
      }
    },

    scrollToElement: function(element, callback)
    {
      if(element == null) { return; }

      this.scrollToPosition($(element).offset().top - 1.1*window.vBook.getTopNavBarHeight(), callback);
    },

    getDistanceTopFromViewport: function(element)
    {
      if(element == null){ return 0; }

      return element.getBoundingClientRect().top
    },

    getTextFragmentElements: function getTextFragmentElements()
    {
      var textElements = document.querySelectorAll("#bookMarkupContainer .textItemContainer");
      var textElementsFiltered = [];

      for(var i = 0; i < textElements.length; i++)
      {
        var textElement = textElements[i];
        var $textElement = $(textElement);

        if($textElement.children(".singleImageParagraph").length > 0)
        {
          continue;
        }

        textElementsFiltered.push(textElement);
      }

      return textElementsFiltered;
    },

    getTextElementPositionPointTop: function getTextElementPositionPointTop($element)
    {
      var top = $element.position().top - this.getElementLineHeight($element)/3.5;

      if($element.find("h1").length > 0) { top += this.getElementLineHeight($element)/2; }
      if($element.find("h2").length > 0) { top += this.getElementLineHeight($element)/11; }
      if($element.find("pre").length > 0) { top += this.getElementLineHeight($element);}
      if($element.is(".questionsGroupContainer"))
      {
        var $buttonElement = $element.find(".button-questions");
        top = $element.position().top + 1.5*$buttonElement.height();
      }

      return top;
    },

    getElementLineHeight: function getElementLineHeight($element)
    {
      return parseFloat($element.css("line-height"));
    },

    isElementVisible: function isElementVisible(element)
    {
      if(element == null) { return false; }

      var rect = element.getBoundingClientRect();
      var $window = $(window);

      return rect.top >= window.vBook.getTopNavBarHeight() && rect.left >= 0
          && rect.bottom <= ($window.height() - 100)
          && rect.right <=  $window.width();
    },

    isElementVisibleEvenALittle: function(element)
    {
      if(element == null) { return false; }

      var rect = element.getBoundingClientRect();
      var elementHeight = rect.height;
      var $window = $(window);

      return rect.top + 0.82*elementHeight >= window.vBook.getTopNavBarHeight()
          && rect.bottom <= $window.height() + 0.7*elementHeight
          && rect.left >= 0
          && rect.right <=  $window.width();
    },

    isAtLeastPartOfElementVisible: function(element)
    {
      if(element == null) { return false; }

      var rect = element.getBoundingClientRect();
      var $window = $(window);

      return (rect.top >= 0 || rect.bottom <= $window.height())
          && (rect.right <=  $window.width() || rect.left >= 0);
    },

    canBothElementsFitIntoScreen: function(element1, element2)
    {
      var $element1 = $(element1);
      var $element2 = $(element2);

      var element1Start = $element1.offset().top;
      var element2Start = $element2.offset().top;

      var element1End = element1Start + $element1.height();
      var element2End = element2Start + $element2.height();

      var top = Math.min(element1Start, element2Start);
      var bottom = Math.max(element1End, element2End);

      return (bottom - top) < (0.9 * $(window).height());
    },

    areElementsVisible: function (elements)
    {
      for(var i = 0; i < elements.length; i++)
      {
        if(!this.isElementVisible(elements[i])){ return false; }
      }

      return true;
    },

    isFirstCloserToTheTopOfThePage: function($firstElement, $secondElement)
    {
      return $firstElement.offset().top < $secondElement.offset().top;
    },

    precedesElement: function precedesElement(a, b)
    {
      return a.compareDocumentPosition(b) & Node.DOCUMENT_POSITION_PRECEDING;
    },

    followsElement: function followsElement(a, b)
    {
      return a.compareDocumentPosition(b) & Node.DOCUMENT_POSITION_FOLLOWING;
    },

    isContained: function isContained(a, b)
    {
      var comparison = a.compareDocumentPosition(b);

      return comparison & Node.DOCUMENT_POSITION_CONTAINS
          || comparison & Node.DOCUMENT_POSITION_CONTAINED_BY;
    },
    findFirstPrevious: function($element, selector)
    {
      if($element == null) { return $(null); }

      var $currentElement = $element;

      while(($currentElement = $currentElement.prev()) && $currentElement.length > 0)
      {
        if($currentElement.is(selector)) { return $currentElement; }

        if($currentElement.is("ul, .noteTipSection"))
        {
          var $lastChild = $currentElement.children().last();

          if($lastChild.is(selector)) { return $lastChild; }

          var $found = this.findFirstPrevious($lastChild, selector);
          if($found.length > 0)
          {
            return $found;
          }
        }
      }

      return $(null);
    },

    findFirstNext: function($baseElement, selector)
    {
      var $currentElement = $baseElement;

      while($currentElement = $currentElement.next())
      {
        if($currentElement.is(selector)) { return $currentElement; }
      }

      return $(null);
    }
  }
};

$(document).ready(function() {

  prettyPrint();

  var initialElement;

  if(window.location.hash && window.location.hash != "#")
  {
    initialElement = $(window.location.hash).parents(".textItemContainer").get(0);

    window.location.hash = "";
  }

  if(!initialElement)
  {
    initialElement = $("#bookMarkupContainer h1").parents(".textItemContainer").get(0);
  }

  setTimeout(function()
  {
    $(document).trigger
    (
      "playingTextFragmentChange",
      initialElement
    );
  }, window.vBook.pauseTime);

  /****************** ANCHOR NAVIGATION ************************/
  window.vBook.handleBrowsableLinkHandler = function(event)
  {
    var $this = $(this);
    var text = $this.text();

    var isListingOrFigure =  text.match(window.vBook.listingRegEx)
                          || text.match(window.vBook.figureRegEx);

    var scrollToElement = this.hash ? document.querySelector(this.hash)
                                    : null;

    if(isListingOrFigure)
    {
      var $textFragment = $(this).parents(".textItemContainer");
      var textFragment = $textFragment[0];

      var browsable = window.vBook.utils.getMatchingBrowsable(textFragment.browsables, $this.text());

      $(browsable).closest(".focus-off").removeClass("focus-off");
      $(browsable.heading).closest(".focus-off").removeClass("focus-off");

      if(browsable)
      {
        var canFitIntoScreen = window.vBook.utils.canBothElementsFitIntoScreen(textFragment, browsable);

        if(!canFitIntoScreen)
        {
          $(document).trigger("moveBrowsableRequested", [textFragment, browsable, false]);
          return false;
        }
        else if(window.vBook.utils.precedesElement(scrollToElement, textFragment))
        {
          scrollToElement = textFragment;
        }
      }
    }

    if(scrollToElement)
    {
      window.vBook.utils.scrollToElement(scrollToElement);
      event.preventDefault();
    }

    return false;
  };

  $('a[href*=#]').on('click', window.vBook.handleBrowsableLinkHandler);
});
