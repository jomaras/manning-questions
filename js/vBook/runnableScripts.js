/*
  events:
    - document: textLayoutModified
*/

$(document).ready(function()
{
  var editPreviewFrame = document.getElementById("iframe-edit-preview");
  var lastEditedCode = "";

  var $modalEditCode = $("#modal-edit-code");

  var template = $("#execution-environment-template").text();

  addExecutionPanels(template);

  function addExecutionPanels(executionPanelTemplate)
  {
    var $runnableScripts = $("#bookMarkupContainer .script-runnable");

    for(var i = 0; i < $runnableScripts.length; i++)
    {
      var runnableScript = $runnableScripts[i];
      var $runnableScript = $(runnableScript);

      var $executionEnvironment = $(executionPanelTemplate);

      $runnableScript.is("form") ? $executionEnvironment.insertAfter($runnableScript)
                                 : $executionEnvironment.insertAfter($runnableScript.parents(".textItemContainer"));

      var $editButton = $executionEnvironment.find(".button-edit");
      var $toggleResultsButton = $executionEnvironment.find(".button-toggle-result");

      $toggleResultsButton.on("click", handleMainContentToggleResultsPress);
      $editButton.on("click", handleEditButtonPress);

      $toggleResultsButton[0].runnableScript = runnableScript;
      $editButton[0].runnableScript = runnableScript;
    }
  };

  function handleMainContentToggleResultsPress()
  {
    var $toggleResultsButton = $(this);
    var $executionContainer = $toggleResultsButton.parents(".execution-environment-container");
    var $iFrame = $executionContainer.find(".execution-environment-frame");

    if(!$toggleResultsButton.is(".opened"))
    {
      asyncGetRunnableScript(this.runnableScript, function(code)
      {
        setIframeContent($iFrame, code);
        $iFrame.velocity("slideDown", function()
        {
          $(document).trigger("textLayoutModified", $executionContainer.get(0));
        });

        $toggleResultsButton.addClass("opened")
                            .find("span")
                            .text("Hide result");
      });
    }
    else
    {
      $iFrame.velocity("slideUp", function()
      {
        $(document).trigger("textLayoutModified", $executionContainer.get(0));
      });

      $toggleResultsButton.removeClass("opened")
                          .find("span")
                          .text("Show result");
    }

    return false;
  }

  function handleEditButtonPress()
  {
    if(this.runnableScript == null) { return false; }

    asyncGetRunnableScript(this.runnableScript, function(code)
    {
      var textArea = $modalEditCode.find(".model-edit-code-text")[0];
      textArea.value = code;

      var codeMirror = CodeMirror.fromTextArea(textArea, { mode:  "htmlmixed", lineNumbers: true });

      $modalEditCode.foundation('reveal', 'open');

      lastEditedCode = code;
    });

    return false;
  }

  $("#modal-edit-code .button-run").on("click", function()
  {
    var codeMirror = getCodeMirror();
    if(codeMirror == null){ return; }

    setIframeContent($(editPreviewFrame), codeMirror.getValue());

    return false;
  });

  $("#modal-edit-code .button-reset").on("click", function()
  {
    var codeMirror = getCodeMirror();
    if(codeMirror == null){ return; }

    codeMirror.setValue(lastEditedCode);
    setIframeContent($(editPreviewFrame), lastEditedCode);

    return false;
  });

  $(document).on('opened.fndtn.reveal', '[data-reveal]', function ()
  {
    var codeMirror = getCodeMirror();
    if(codeMirror == null) { return; }

    codeMirror.refresh();

    setIframeContent($(editPreviewFrame), codeMirror.getValue());
  });

  function setIframeContent($iFrame, code)
  {
    var iFrame = $iFrame[0];
    iFrame.contentWindow.contents = code;
    iFrame.src = 'javascript:window["contents"]';
  }

  $(document).on('closed.fndtn.reveal', '[data-reveal]', function ()
  {
    $('.CodeMirror').remove();
  });

  function getCodeMirror()
  {
    var $codeMirrors = $('.CodeMirror');

    if($codeMirrors.length == 0) { return null; }

    return $codeMirrors[0].CodeMirror;
  }

  function asyncGetRunnableScript(runnableScriptElement, callback)
  {
    if(runnableScriptElement == null){ return; }

    var listingFileName = $(runnableScriptElement).attr("location");

    $.ajax
    ({
      url: "resources/books/Secrets_of_the_JavaScript_Ninja/Listings/" + listingFileName
    }).done(callback);
  }
});
