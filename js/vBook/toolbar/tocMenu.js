
$(document).ready(function()
{
  var titleElements = document.querySelectorAll("h1, h2, h3:not(.introHeader)");

  var $menuToc = $("#menu-toc");
  var $chapterItems = $menuToc.find(".book-chapter");
  var $toggleButtons = $menuToc.find(".toc-toggle-button");

  var $menuTocButton = $("#toc-menu-button");
  var $tocPositionPoints = $(".toc-position-point");

  $(document).on("audioBookPlaying", function()
  {
    $tocPositionPoints.addClass("pause-point");
  });

  $(document).on("audioBookPause", function()
  {
    $tocPositionPoints.removeClass("pause-point");
  });

  $toggleButtons.on("click", function()
  {
    var $this = $(this);
    var $bookChapterItem = $this.closest(".book-chapter");
    var $sectionsContainer = $bookChapterItem.find(".book-sections-list");

    if($bookChapterItem.is(".opened"))
    {
      $.Velocity.animate($sectionsContainer, "slideUp", "normal").then(function()
      {
        $bookChapterItem.removeClass("opened");
      });
    }
    else
    {
      $.Velocity.animate($sectionsContainer, "slideDown", "fast").then(function()
      {
        $bookChapterItem.addClass("opened");
      });
    }
  });

  $(document).on("playingTextFragmentChange", function(e, newElement)
  {
    if(newElement == null) { return; }

    var currentChapterTocSectionList = document.querySelectorAll(".book-chapter.contains-current-section .book-section");

    var currentSectionElement = getCurrentSectionElement(newElement);

    if(currentSectionElement == null) { return; }

    var currentMarkedSectionElement = document.querySelector(".book-chapter.contains-current-section .current-section");
    if(currentMarkedSectionElement) { $(currentMarkedSectionElement).removeClass("current-section"); }

    var currentSectionTitle = currentSectionElement.textContent.trim();

    for(var i = 0; i < currentChapterTocSectionList.length; i++)
    {
      var currentTocSectionItem = currentChapterTocSectionList[i];

      if(currentTocSectionItem.textContent.trim() == currentSectionTitle)
      {
        $(currentTocSectionItem).addClass("current-section");
        return;
      }
    }
  });

  function getCurrentSectionElement(currentlyPlayingElement)
  {
    if(currentlyPlayingElement == null){ return null;}

    var $currentlyPlayingElement = $(currentlyPlayingElement);

    if($currentlyPlayingElement.find("h2").length > 0) { return currentlyPlayingElement; }

    for(var i = titleElements.length - 1; i >= 0; i--)
    {
      var currentElement = titleElements[i];
      if(window.vBook.utils.isContained(currentlyPlayingElement, currentElement)
      || $(currentElement).is("h3") > 0)
      {
        continue;
      }

      if(window.vBook.utils.precedesElement(currentlyPlayingElement, currentElement))
      {
        return currentElement;
      }
    }

    return null;
  }

  $menuToc.on("mousewheel", function(e)
  {
    var event = e.originalEvent,
    d = event.wheelDelta || -event.detail;

    this.children[0].scrollTop += ( d < 0 ? 1 : -1 ) * 30;

    e.preventDefault();
  });

  $menuTocButton.on("mouseover", function()
  {
    openTocMenu();
    return false;
  });

  $menuTocButton.on("click", function()
  {
    //closeTocMenu();
    return false;
  });

  $(document).on("mouseup", function(e)
  {
    if (!$menuToc.is(e.target) && !$menuTocButton.is(e.target)
    && $menuToc.has(e.target).length === 0 && $menuTocButton.has(e.target).length === 0)
    {
      closeTocMenu();
    }
  });

  function openTocMenu()
  {
    $menuToc.addClass("open f-open-dropdown");
    $menuTocButton.addClass("selected");
  }

  function closeTocMenu()
  {
    $menuToc.removeClass("open f-open-dropdown");
    $menuTocButton.removeClass("selected");
  }

  $tocPositionPoints.on("click", function()
  {
    var $this = $(this);
    $this.is(".pause-point") ? window.vBook.pauseAudioBook()
                             : window.vBook.playAudioBook();
  });
});
