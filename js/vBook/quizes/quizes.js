$(document).ready(function()
{
  var $questionGroups = $(window.vBook.utils.getQuestionContainers()),
      $arrangeCodeInputs = $('.arrange-code-inputs');

  $questionGroups.css("overflow", "hidden");

  $questionGroups.each(function(index, questionGroup)
  {
    var previousTextFragment = window.vBook.utils.findFirstPrevious($(questionGroup), ".textItemContainer");

    questionGroup.isQuiz = true;

    if(previousTextFragment)
    {
      previousTextFragment.quizElement = questionGroup;
    }
  });

  $(".button-questions").on("click", function()
  {
    var $this = $(this);

    var $questionsGroupContainer = $this.parents(".questionsGroupContainer");
    var $questionsContainer = $questionsGroupContainer.find(".questions-container");

    $questionsGroupContainer.removeClass("focus-off");

    if($questionsContainer.is(".collapsed"))
    {
      $questionsGroupContainer.css({overflow: "visible", "max-height": "none"});
      $questionsContainer.removeClass("collapsed");

      $this.parents('.questionsGroupContainer').addClass("questions-opened");
      $(document).trigger("exercisePanelOpened");
    }
    else
    {
      $questionsContainer.addClass("collapsed");
      $this.parents('.questionsGroupContainer').removeClass("questions-opened");
      $(document).trigger("exercisePanelClosed");

      setTimeout(function()
      {
        if($questionsContainer.is(".collapsed"))
        {
          $questionsGroupContainer.css({overflow: "hidden", "max-height": "100px"});

          if($this.is(".inner-quiz-button"))
          {
            window.vBook.utils.scrollToElement($questionsGroupContainer.eq(0));
          }

          $(document).trigger("textLayoutModified", $questionsGroupContainer[0]);
        }
      }, 800);
    }

    return false;
  });

  var $questionContainers = $(".question-container");

  for(var i = 0; i < $questionContainers.length; i++)
  {
    handleExplainationAndTips($($questionContainers[i]))
  }

  $arrangeCodeInputs.each(function () {
      var $arrangeCodePreElement = $(this),
          $questionContainer = $arrangeCodePreElement.parents('.question-container'),
          $arrangeCodeFragments = $questionContainer.find(".arrange-code-fragments");
      parseArrangeCodeQuestions($arrangeCodePreElement, $arrangeCodeFragments);
  });

  /************************ CHOOSE ONE QUESTIONS ******************/
  (function handleChooseOneQuestions()
  {
    var $chooseOnes = $(".question-container.choose-one");

    $chooseOnes.find(".question-answer > input, .question-answer > label")
               .on("click", function()
               {
                 $(document).trigger("exerciseAnswerGiven");
               });

    $chooseOnes.find(".button-check").on("click", function()
    {
      var $this = $(this);
      var $questionContainer = $this.parents(".question-container");

      resetChooseOne($questionContainer);

      var $checkedAnswer = $questionContainer.find("input:checked");
      var $questionAnswer = $checkedAnswer.parents(".question-answer");

      showCorrectChooseOne($questionContainer);

      if(!$questionAnswer.find(".correct").length)
      {
        $questionAnswer.find(".correct-status").addClass("show");
        $(document).trigger("quizAnsweredIncorrectly");
      }
      else
      {
        $(document).trigger("quizAnsweredCorrectly");
      }

      return false;
    });

    $chooseOnes.find(".button-show-answer").on("click", function()
    {
      var $this = $(this);
      var $questionContainer = $this.parents(".question-container");

      resetChooseOne($questionContainer);
      showCorrectChooseOne($questionContainer);

      $(document).trigger("exerciseAnswersShown");

      return false;
    });

    $chooseOnes.find(".button-reset").on("click", function()
    {
      var $this = $(this);
      var $questionContainer = $this.parents(".question-container");

      resetChooseOne($questionContainer);

      $questionContainer.find("input").prop('checked', false);

      $(document).trigger("exerciseReset");

      return false;
    });

    function resetChooseOne($questionContainer)
    {
      $questionContainer.find(".correct-status").removeClass("show");
    }

    function showCorrectChooseOne($questionContainer)
    {
      $questionContainer.find(".correct-status.correct").addClass("show");
    }
  })();
  /************************ END CHOOSE ONE QUESTIONS ******************/


  /************************ START MATCH ITEMS AND DESCRIPTIONS *******/
  (function handleMatchItemsAndDescriptions()
  {
    var $matchItemsDescriptions = $(".question-container.match-items-description");

    for(var i = 0; i < $matchItemsDescriptions.length; i++)
    {
      (function($matchItemsToDescription)
      {
        var $draggableItems = $matchItemsToDescription.find(".question-draggable-item");
        var $draggableItemsPlaceholders = $matchItemsToDescription.find(".question-description-item-place");
        var $descriptionItems = $matchItemsToDescription.find(".question-description-item");

        $draggableItems.draggable
        ({
          scroll: false,
          revert: "invalid",
          containment: "document",
          cursor: "move",
          start: function( event, ui )
          {
            $(this).addClass('drag-active');
          },
          stop: function( event, ui )
          {
            $(this).removeClass('drag-active');
          }
        });

        $draggableItemsPlaceholders.droppable
        ({
          accept: function(el)
          {
            return el.is(".question-draggable-item");
          },
          activeClass: "drop-active",
          drop: function(ev, ui)
          {
            var $this = $(this);
            var $children = $this.children();
            var $draggedInElement = $(ui.draggable);

            if($children.length != 0)
            {
              var $child = $children.eq(0);
              var $draggedInElementParent = $draggedInElement.parent();

              $child.fadeOut(function()
              {
                if($draggedInElementParent.is(".question-draggable-items"))
                {
                  moveDraggableItemToOriginalPlaceholder($child);
                }
                else
                {
                  moveDraggableItemToPlaceholder($child, $draggedInElementParent);
                }

                $child.fadeIn();
              });
            }

            moveDraggableItemToPlaceholder($draggedInElement, $this);

            $(document).trigger("exerciseAnswerGiven");
          }
        });

        $matchItemsToDescription.find(".question-draggable-items").droppable
        ({
          accept: ".question-draggable-item",
          activeClass: "drop-active",
          drop: function(ev, ui)
          {
            moveDraggableItemToOriginalPlaceholder($(ui.draggable));
          }
        });

        $matchItemsToDescription.find(".button-check").on("click", function()
        {
          resetCorrectIncorrectLabels();

          var numberOfIncorrectAnswers = 0;

          $descriptionItems.each(function(index, descriptionItem)
          {
            var $descriptionItem = $(descriptionItem);
            var $draggedInItem = $descriptionItem.find(".question-draggable-item");
            var $placeholder = $descriptionItem.find(".question-description-item-place");

            var statusClass = $draggedInItem.attr("index") == $placeholder.attr("matchingindex")
                            ? "correct"
                            : "incorrect";

            if(statusClass == "incorrect"){ numberOfIncorrectAnswers++; }

            $descriptionItem.find(".correct-status")
                            .addClass(statusClass)
                            .addClass("show");
          });

          numberOfIncorrectAnswers > 0 ? $(document).trigger("quizAnsweredIncorrectly")
                                       : $(document).trigger("quizAnsweredCorrectly")

          return false;
        });

        $matchItemsToDescription.find(".button-reset").on("click", function()
        {
          var $droppedItems = $matchItemsToDescription.find(".question-description-item-place .question-draggable-item");

          if($droppedItems.length == 0){ return false; }

          $droppedItems.fadeOut(function()
          {
            resetCorrectIncorrectLabels();

            $droppedItems.each(function(item, element)
            {
              moveDraggableItemToOriginalPlaceholder($(element));
            });

            $droppedItems.fadeIn();
          });

          $(document).trigger("exerciseReset");

          return false;
        });

        $matchItemsToDescription.find(".button-show-answer").on("click", function()
        {
          resetCorrectIncorrectLabels();

          $draggableItems.fadeOut(function()
          {
            $draggableItemsPlaceholders.each(function(index, placeholder)
            {
              var $placeholder = $(placeholder);
              var $draggableItem = $(getDraggableItemWithIndex($placeholder.attr("matchingindex")));

              moveDraggableItemToPlaceholder($draggableItem, $placeholder);
            });

            $draggableItems.fadeIn();
          });

          $(document).trigger("exerciseAnswersShown");

          return false;
        });

        function getDraggableItemWithIndex(index)
        {
          for(var i = 0; i < $draggableItems.length; i++)
          {
            var draggableItem = $draggableItems[i];

            if($(draggableItem).attr("index") == index){ return draggableItem; }
          }

          return null;
        }

        function moveDraggableItemToPlaceholder($item, $placeholder)
        {
          $item.detach().css({top: "2px",left: "2px"}).appendTo($placeholder);
        }

        function moveDraggableItemToOriginalPlaceholder($item)
        {
          $item.detach().css({top: "2px", left: "2px"}).appendTo
          (
            $matchItemsToDescription.find(".question-draggable-items")
          );
        }

        function resetCorrectIncorrectLabels()
        {
          $descriptionItems.find(".correct-status").removeClass("show")
                                                   .removeClass("correct")
                                                   .removeClass("incorrect");
        }

      })($($matchItemsDescriptions[i]));
    }
  })();
  /************************ END MATCH ITEMS AND DESCRIPTIONS *******/

  /************************ HANDLE CHOOSE MULTIPLE *****************/
  (function handleChooseMultiple()
  {
    var $chooseMultiples = $(".question-container.choose-multiple");

    for(var i = 0; i < $chooseMultiples.length; i++)
    {
        (function($chooseMultipleContainer)
        {
          var $draggableItems = $chooseMultipleContainer.find(".draggable-option-container");
          var $targetContainer = $chooseMultipleContainer.find(".answers-container");
          var $sourceContainer = $chooseMultipleContainer.find(".options-container");

          var sourceContainerHeight = $sourceContainer.height();

          $sourceContainer.css
          ({
             "min-height": sourceContainerHeight + "px"
          });

          $targetContainer.css
          ({
             "min-height": sourceContainerHeight + "px"
          });

          $draggableItems.draggable
          ({
            scroll: false,
            revert: "invalid",
            containment: "document",
            cursor: "move",
            start: function( event, ui )
            {
              $(this).addClass('drag-active');
            },
            stop: function( event, ui )
            {
              $(this).removeClass('drag-active');
            }
          });

          $targetContainer.droppable
          ({
            accept: ".draggable-option-container",
            activeClass: "drop-active",
            drop: function(ev, ui)
            {
              moveElementToDestination(ui.draggable);
              $(document).trigger("exerciseAnswerGiven");
            }
          });

          $sourceContainer.droppable
          ({
            accept: ".draggable-option-container",
            activeClass: "drop-active",
            drop: function(e, ui)
            {
              moveElementToSource(ui.draggable);
            }
          });

          $chooseMultipleContainer.find(".button-check").on("click", function()
          {
            reset();

            $targetContainer.find(".correct-status").addClass("show");

            $sourceContainer.find(".correct-status.correct").addClass("show");

            if($targetContainer.find(".correct-status.incorrect").length == 0
            && $sourceContainer.find(".correct-status.correct").length == 0)
            {
              $(document).trigger("quizAnsweredCorrectly");
            }
            else
            {
              $(document).trigger("quizAnsweredIncorrectly");
            }

            return false;
          });

          $chooseMultipleContainer.find(".button-reset").on("click", function()
          {
            reset();

            var $draggableOptions = $targetContainer.find(".draggable-option-container");

            $draggableOptions.fadeOut(function()
            {
              $draggableOptions.each(function(index, element)
              {
                moveElementToSource(element);
              });

              $draggableOptions.fadeIn();
            });

            $(document).trigger("exerciseReset");

            return false;
          });

          $chooseMultipleContainer.find(".button-show-answer").on("click", function()
          {
            reset();

            var $draggableOptions = $targetContainer.find(".draggable-option-container");

            var $incorrectDragged = $($targetContainer.find(".draggable-option-container .incorrect").map(function(index, element)
            {
              return $(element).parents(".draggable-option-container")[0];
            }));

            $incorrectDragged.fadeOut(function()
            {
              $incorrectDragged.each(function(index, element)
              {
                moveElementToSource(element);
              })

              $incorrectDragged.fadeIn();
            });

            var $correctNotDragged = $($sourceContainer.find(".draggable-option-container .correct").map(function(index, element)
            {
              return $(element).parents(".draggable-option-container")[0];
            }));

            $correctNotDragged.fadeOut(function()
            {
              $correctNotDragged.each(function(index, element)
              {
                moveElementToDestination(element);
              })

              $correctNotDragged.fadeIn();
            });

            $(document).trigger("exerciseAnswersShown");

            return false;
          });

          function reset()
          {
            $chooseMultipleContainer.find(".correct-status").removeClass("show");
          }

          function moveElementToSource(element)
          {
            $(element).detach().css({top: 0, left: 0}).appendTo($sourceContainer);
          }

          function moveElementToDestination(element)
          {
            $(element).detach().css({top: 0, left: 0}).appendTo($targetContainer);
          }
        })($($chooseMultiples[i]))
    }

  })();
  /************************ END CHOOSE MULTIPLE ********************/

  /************************ START GIVE ANSWER QUESTIONS ******************/
  (function handleGiveAnswerQuestions() {
      var $giveAnswer = $(".question-container.give-answer");

      $giveAnswer.find(".button-check").on("click", function () {
          var $this = $(this),
              $questionContainer = $this.parents('.question-container'),
              $correctStatusContainer = $questionContainer.find(".correct-status"),
              $input = $questionContainer.find('.give-answer-input'),
              isCorrectAnswerGiven = $input.val() == $input.attr('correctAnswer');

          resetGiveAnswerCheckmark($input);

          if(isCorrectAnswerGiven) {
            $correctStatusContainer.addClass("correct");
            $(document).trigger("quizAnsweredCorrectly");
          }
          else {
            $correctStatusContainer.addClass("incorrect");
            $(document).trigger("quizAnsweredIncorrectly");
          }

          $correctStatusContainer.addClass("show");

          return false;
      });

      $giveAnswer.find(".button-show-answer").on("click", function () {
          var $this = $(this),
              $questionContainer = $this.parents(".question-container"),
              $input = $questionContainer.find('.give-answer-input');

          resetGiveAnswerCheckmark($input);
          showCorrectGiveAnswer($input);

          $(document).trigger("exerciseAnswersShown");

          return false;
      });

      $giveAnswer.find(".button-reset").on("click", function () {
          var $this = $(this),
              $questionContainer = $this.parents(".question-container"),
              $input = $questionContainer.find('.give-answer-input');

          resetGiveAnswerCheckmark($input);
          resetGiveAnswerValue($input);

          $(document).trigger("exerciseReset");

          return false;
      });

      function resetGiveAnswerCheckmark($input) {
          $input.parents(".question-container")
                .find('.correct-status').removeClass('correct incorrect show');
      }

      function resetGiveAnswerValue($input) {
          $input.val('');
      }

      function showCorrectGiveAnswer($input) {
          $input.val($input.attr('correctAnswer'));
          $input.parents(".question-container")
                .find('.correct-status').addClass('correct show');
      }
  })();
  /************************ END GIVE ANSWER QUESTIONS ******************/

    /************************ START ARRANGE CODE QUESTIONS ******************/
    (function handleArrangeCodeQuestions() {
        var $arrangeCodeItems = $(".question-container.arrange-code");

        for (var i = 0; i < $arrangeCodeItems.length; i++) {
            (function ($arrangeCodeToItems) {
                var $draggableItems = $arrangeCodeToItems.find(".code-draggable-item"),
                    $draggableItemsPlaceholders = $arrangeCodeToItems.find(".code-item-placeholder"),
                    $descriptionItems = $arrangeCodeToItems.find(".code-item-placeholder-container");

                $draggableItems.draggable({
                    scroll: false,
                    revert: "invalid",
                    containment: "document",
                    cursor: "move",
                    start: function (event, ui) {
                        $(this).addClass('drag-active');
                    },
                    stop: function (event, ui) {
                        $(this).removeClass('drag-active');
                    }
                });

                $draggableItemsPlaceholders.droppable({
                    accept: function (el) {
                        return el.is(".code-draggable-item");
                    },
                    activeClass: "drop-active",
                    drop: function (ev, ui) {
                        var $this = $(this);
                        var $children = $this.children();
                        var $draggedInElement = $(ui.draggable);

                        if ($children.length != 0) {
                            var $child = $children.eq(0);
                            var $draggedInElementParent = $draggedInElement.parent();

                            $child.fadeOut(function () {
                                if ($draggedInElementParent.is(".arrange-code-fragments")) {
                                    moveDraggableItemToOriginalPlaceholder($child);
                                } else {
                                    moveDraggableItemToPlaceholder($child, $draggedInElementParent);
                                }

                                $child.fadeIn().css({display: "inline-block"});
                            });
                        }

                        moveDraggableItemToPlaceholder($draggedInElement, $this);

                        $(document).trigger("exerciseAnswerGiven");
                    }
                });

                $arrangeCodeToItems.find(".arrange-code-fragments").droppable({
                    accept: ".code-draggable-item",
                    activeClass: "drop-active",
                    drop: function (ev, ui) {
                        moveDraggableItemToOriginalPlaceholder($(ui.draggable));
                    }
                });

                $arrangeCodeToItems.find(".button-check").on("click", function () {
                    resetCorrectIncorrectLabels();

                    var numberOfIncorrectAnswers = 0;

                    $descriptionItems.each(function (index, descriptionItem) {
                        var $descriptionItem = $(descriptionItem);
                        var $draggedInItem = $descriptionItem.find(".code-draggable-item");
                        var $placeholder = $descriptionItem.find(".code-item-placeholder");

                        var statusClass = $draggedInItem.text() == $placeholder.attr('correct-answer')
                                        ? "correct"
                                        : "incorrect";

                        if (statusClass == "incorrect") {
                            numberOfIncorrectAnswers++;
                        }

                        $descriptionItem.find(".correct-status")
                                        .addClass(statusClass)
                                        .addClass("show");
                    });

                    numberOfIncorrectAnswers > 0 ? $(document).trigger("quizAnsweredIncorrectly")
                                                 : $(document).trigger("quizAnsweredCorrectly")

                    return false;
                });

                $arrangeCodeToItems.find(".button-reset").on("click", function () {
                    var $droppedItems = $arrangeCodeToItems.find(".code-item-placeholder .code-draggable-item");

                    if ($droppedItems.length == 0) {
                        return false;
                    }

                    $droppedItems.fadeOut(function () {
                        resetCorrectIncorrectLabels();

                        $droppedItems.each(function (item, element) {
                            moveDraggableItemToOriginalPlaceholder($(element));
                        });

                        //fade in goes to block, and we need inline-block
                        $droppedItems.fadeIn().css({display: "inline-block"});
                    });

                    $(document).trigger("exerciseReset");

                    return false;
                });

                $arrangeCodeToItems.find(".button-show-answer").on("click", function() {
                    resetCorrectIncorrectLabels();

                    $draggableItems.fadeOut(function()
                    {
                        $draggableItemsPlaceholders.each(function(index, placeholder)
                        {
                            var $placeholder = $(placeholder);
                            var $draggableItem = $(getDraggableItemWithIndex($placeholder.attr("correct-answer")));

                            moveDraggableItemToPlaceholder($draggableItem, $placeholder);
                        });

                        $draggableItems.fadeIn().css({display: "inline-block"});
                    });

                    $(document).trigger("exerciseAnswersShown");

                    return false;
                });

                function getDraggableItemWithIndex(correctAnswer) {
                    for (var i = 0; i < $draggableItems.length; i++) {
                        var draggableItem = $draggableItems[i];

                        if ($(draggableItem).text() == correctAnswer) {
                            return draggableItem;
                        }
                    }

                    return null;
                }

                function moveDraggableItemToOriginalPlaceholder($item) {
                    $item.detach()
                         .css({top: "2px", left: "2px"})
                         .appendTo($arrangeCodeToItems.find(".arrange-code-fragments"));
                }

                function moveDraggableItemToPlaceholder($item, $placeholder) {
                    $item.css({top: 0, left: 0});
                    $item.detach().appendTo($placeholder);
                }

                function resetCorrectIncorrectLabels() {
                    $descriptionItems.find(".correct-status")
                        .removeClass("show")
                        .removeClass("correct")
                        .removeClass("incorrect");
                }

            })($($arrangeCodeItems[i]));
        }
    })();
    /************************ END ARRANGE CODE QUESTIONS ******************/

  function parseArrangeCodeQuestions($arrangeCodePreElement, $arrangeCodeFragments) {
      var startDelimiterFound = false,
          endDelimiterFound = false,
          removeElementsList = [],
          correctAnswerValue,
          $spanBoxInput = null;

      var $startAnswerElement = $(null),
          $endAnswerElement = $(null);
      var $spans = $arrangeCodePreElement.find('span');

      $spans.each(function()
      {
        var $span = $(this);
        var $codeFragment = null;

        if ($span.text() == '{.') {
            $startAnswerElement = $span;
        }
        else if ($span.text() == '.}') {
            $endAnswerElement = $span;
            var $contentElements = getSubsetBetweenExclusive($spans, $startAnswerElement, $endAnswerElement);

            correctAnswerValue = getJointText($contentElements).trim();

            $codeFragment = $("<span class='code-draggable-item'></span>");
            $codeFragment.text(correctAnswerValue);
            $arrangeCodeFragments.append($codeFragment);

            $spanBoxInput = $("<span class='code-item-placeholder-container'>" +
                                    "<span class='code-item-placeholder' correct-answer=''></span>" +
                                    "<span class='correct-status'><i></i></span>" +
                                "</span>");
            $spanBoxInput.insertBefore($endAnswerElement);
            $spanBoxInput.find('.code-item-placeholder')
                         .attr('correct-answer', correctAnswerValue)
                         .text(correctAnswerValue.replace(/./g, " ") + " ");

            $startAnswerElement.remove();
            $endAnswerElement.remove();
            $contentElements.remove();
        }
      });
  }

  function getJointText($collection){
    if($collection == null || $collection.length == 0) { return ""; }

    var text = "";

    $collection.each(function(index, element){
      text += $(element).text();
    });

    return text;
  }

  function getSubsetBetweenExclusive($collection, $startElement, $endElement)
  {
    var results = [];

    if($collection != null && $collection.length > 0) {
      var startIndex = $collection.index($startElement);
      var endIndex = $collection.index($endElement);

      if(startIndex >= 0 && endIndex >= 0 && endIndex > startIndex) {
        for(var i = startIndex + 1; i < endIndex; i++) {
          results.push($collection[i]);
        }
      }
    }

    return $(results);
  }

  function handleExplainationAndTips($container)
  {
    var isExplainShown = false, isHintShown = false;
    $container.find(".button-explain").on("click", function()
    {
      if(isExplainShown)
      {
        closeExplanation();
        return false;
      }

      $container.find(".explanation-container").velocity("slideDown");
      isExplainShown = true;

      $(document).trigger("exerciseHintExplanationOpened");
      $(document).trigger("textLayoutModified", $container[0]);

      return false;
    });

    $container.find(".explanation-container .close-question-tip").on("click", function()
    {
      closeExplanation();
      return false;
    });

    $container.find(".button-hint").on("click", function()
    {
      if(isHintShown)
      {
        closeHint();
        return false;
      }

      $container.find(".hint-container").velocity("slideDown");
      isHintShown = true;

      $(document).trigger("exerciseHintExplanationOpened");
      $(document).trigger("textLayoutModified", $container[0]);

      return false;
    });

    $container.find(".hint-container .close-question-tip").on("click", function()
    {
      closeHint();
      return false;
    });

    function closeExplanation()
    {
      $container.find(".explanation-container").velocity("slideUp");
      $(document).trigger("exerciseHintExplanationClosed");

      isExplainShown = false;
    }

    function closeHint()
    {
      $(document).trigger("exerciseHintExplanationClosed");
      $container.find(".hint-container").velocity("slideUp");

      isHintShown = false;
    }
  }
});
