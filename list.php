<!doctype html>
<html class="no-js" lang="en">
<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>Manning vBook</title>
  <link rel="stylesheet" href="stylesheets/app.css" />
  <link rel="stylesheet" href="stylesheets/quiz.css" />
  <script src="bower_components/modernizr/modernizr.js"></script>
  <style>
  #mainContentContainer{
    margin-top: 30px;
  }
  </style>
</head>
<body>
  <div class="fixed">
    <nav class="top-bar" id="top-bar" data-topbar role="navigation">
      <section class="top-bar-section">
        <ul class="left">
          <li>
            <a href="#">
              <i class="icon-i-manning-m"></i>
            </a>
          </li>

          <li class="toc has-dropdown">
            <a href="#" id="toc-menu-button">
              <i class="icon-i-toc"></i>
            </a>
          </li>
        </ul>
      </section>
    </nav>
  </div>

  <div class="row" id="main-row">
    <div class="large-12 columns">

      <div class="text-wrapper">
        <div id="mainContentContainer">
          <?php

            $folderNames = getFolderNames('resources/books/');

            echo("<ul>");
            foreach ($folderNames as $folderName) 
            {
              echo("<li><a href='questions.php?book=$folderName'>$folderName</a></li>");
            }
            echo("</ul>");
            

            function getFolderNames($rootFolder)
            {
              $directories = array();

              if ($handle = opendir($rootFolder)) 
              {
                while (false !== ($entry = readdir($handle))) 
                {
                    if(strpos($entry, ".") === 0) { continue; }

                    $directories[] = $entry;
                }

                closedir($handle);
              }

              return $directories;
            }
          ?>
        </div> <!-- #mainContentContainer -->

      </div><!-- /.text-wrapper -->

    </div><!-- /.columns -->
  </div><!-- /.row -->
</body>
</html>
