{
  "questionGroup0":
  [
    {
      "index": 1,
      "type": "matchItemsToDescription",
      "text": "Match each concept to its description.",
      "description": "Match items",
      "options":
      [
        { "value": "Concurrency"},
        { "value": "Parallelism"}
      ],
      "descriptions":
      [
        { "value": "Several tasks are performed simultaneously by different processor cores.", "matchingIndex": 1},
        { "value": "Several tasks are performed simultaneously, in a non-blocking way, on the same processor core.", "matchingIndex": 0}
      ],
      "explanation": "As shown in figure 11.2, concurrency involves a series of tasks executed in a non-blocking way. While combining concurrency and parallelism is not impossible, the definition of concurrency does not involve working on more than one thread.",
      "hint": "What is a CompletableFuture meant to address?"
    }
  ],
  "questionGroup1":
  [
    {
      "index": 1,
      "type": "chooseOne",
      "text": "Working with a Future can't guarantee getting a value in a timely manner.",
      "description": "Choose one",
      "options":
      [
        { "value": "True", "isCorrect": true},
        { "value": "False"}
      ],
      "hint": "Look at the overloaded get() method of the Future interface.",
      "explanation": "The get() method has two versions: one taking a long and a TimeUnit, which blocks for at most the specified amount of time but is not guaranteed to return a result, and one taking no parameters, which guarantees to return a result but blocks until there is one available to return... if it ever happens!"
    },
    {
      "index": 2,
      "type": "chooseMultiple",
      "text": "Which of the following statements are valid?",
      "description": "Select all that apply",
      "options":
      [
        { "value": "1. Using asynchronous code involves multithreading.", "isCorrect": true},
        { "value": "2. A CompletableFuture instance can be passed to pre-Java 8 code handling Future instances.", "isCorrect": true},
        { "value": "3. Checking the completion of tasks performed by a set of Futures could be done before Java 8.", "isCorrect": true},
        { "value": "4. Asynchronous tasks could not be chained before Java 8."}
      ],
      "explanation": "If multithreading were to be managed programmatically (i.e. within a single Java thread), statement (1) might be disproved. But this is already the job of the JVM's multithreading framework together with the Thread class, so trying to do that job manually would be totally useless! Since CompletableFuture implements the Future interface, statement (2) is valid provided that the type arguments match (e.g., CompletableFuture<Double> vs. Future<Double>). Statement (3) can be considered valid since this can be done invoking the isDone() method, which is defined in the Future interface. Statement (4) is invalid because chaining asynchronous tasks could be achieved before Java 8. The CompletableFuture class facilitates and helps optimize this job.",
      "hint": "What can be done with one single thread? What is the CompletableFuture class hierarchy? Which methods are defined in the Future interface?"
    }
  ],
  "questionGroup2":
  [
    {
      "index": 1,
      "type": "chooseOne",
      "text": "CompletableFuture is a concrete class that can be instantiated only through public static methods.",
      "description": "Choose one",
      "options":
      [
        { "value": "True"},
        { "value": "False", "isCorrect": true}
      ],
      "hint": "What are the declared CompletableFuture constructors?",
      "explanation": "CompletableFuture is a non-abstract class (this part is true) that has a public no-argument constructor that allows creating instances directly. Listing 11.4 shows an example of it."
    },
    {
      "index": 2,
      "type": "chooseMultiple",
      "text": "Which of the following CompletableFuture method calls are non-blocking?",
      "description": "Select all that apply",
      "options":
      [
        { "value": "(1) get(1L, TimeUnit.HOURS)"},
        { "value": "(2) get()"},
        { "value": "(3) complete(\"done\")", "isCorrect": true},
        { "value": "(4) getNow(\"some value\")", "isCorrect": true},
        { "value": "(4) isDone()", "isCorrect": true}
      ],
      "explanation": "Both get() overloads are blocking: the first one for at most an hour and the second indefinitely (until a result is available). The getNow() method avoids blocking by returning an alternate value passed as an argument if the real result isn't available. The two others can't block as their meaning has nothing to do with retrieving an actual value.",
      "hint": "Check the meaning of each method call and pay attention to any existing overload."
    },
    {
      "index": 3,
      "type": "chooseOne",
      "text": "All CompletableFuture get() overloads may throw checked exceptions.",
      "description": "Choose one",
      "options":
      [
        { "value": "True", "isCorrect": true},
        { "value": "False"}
      ],
      "hint": "Can a blocking method be \"interrupted\"?",
      "explanation": "The get() method may throw an ExecutionException or an InterruptedException. Additionally, the get(long, TimeUnit) overload may throw a TimeoutException. All three are checked exceptions. The non-blocking getNow() method, however, returns a value immediately and doesn't throw any checked exception."
    },
    {
      "index": 4,
      "type": "matchItemsToDescription",
      "text": "Match each CompletableFuture method to what it does.",
      "description": "Match items",
      "options":
      [
        { "value": "allOf(CompletableFuture<T>...)"},
        { "value": "anyOf(CompletableFuture<T>...)"},
        { "value": "completedFuture(T)"},
        { "value": "runAsync(Runnable)"},
        { "value": "supplyAsync(Supplier<T>)"}
      ],
      "descriptions":
      [
        { "value": "Builds a CompletableFuture that will encapsulate the value returned by a given task.", "matchingIndex": 4},
        { "value": "Builds a CompletableFuture that runs a given task without returning any result (except the CompletableFuture itself).", "matchingIndex": 3},
        { "value": "Builds a CompletableFuture that completes as soon as any member of a given set of CompletableFutures completes.", "matchingIndex": 1},
        { "value": "Builds a CompletableFuture that completes when all members of a given set of CompletableFutures complete.", "matchingIndex": 0},
        { "value": "Builds a \"pre-completed\" CompletableFuture encapsulating a given result.", "matchingIndex": 2}
      ],
      "explanation": "The allOf() and anyOf() methods are the easy way to atomically manage a set of CompletableFutures. completedFuture() is a utility method that builds a CompletableFuture with a predefined result. The supplyAsync() and runAsync() methods simplify the process of manually building a CompletableFuture and coding the thread that will complete it (with or without a result). In the latter case, the CompletableFuture returned will encapsulate a null value.",
      "hint": "All these methods are utility methods that simplify the creation of a CompletableFuture."
    }
  ],
  "questionGroup3":
  [
    {
      "index": 1,
      "type": "chooseOne",
      "text": "The following series of operations is faster to complete than its sequential stream()-based counterpart.",
      "description": "Choose one",
      "code": "List<String> prices = shops.parallelStream()\n    .map(shop -> CompletableFuture.supplyAsync(\n    () -> shop.getPrice(product)))\n    .collect(Collectors.toList())",
      "options":
      [
        { "value": "True", "isCorrect": true},
        { "value": "False"}
      ],
      "hint": "This is a combination of laziness and parallelism!",
      "explanation": "While this code may not be optimal when handling more shops than the number of available threads, it is indeed faster than its sequential counterpart."
    },
    {
      "index": 2,
      "type": "chooseMultiple",
      "text": "Refer to Listing 11.11, which sets up a stream of CompletableFutures and gathers their results into a list of Strings. Given the following code (that is, the sequential counterpart of the preceding exercise), what should be changed to make it about as fast as Listing 11.11 without splitting the process into two instructions?",
      "description": "Select all that apply",
      "code": "List<String> prices = shops.stream()\n    .map(shop -> CompletableFuture.supplyAsync(\n    () -> shop.getPrice(product)))\n    .collect(Collectors.toList())",
      "options":
      [
        { "value": "Replacing stream() by parallelStream()", "isCorrect": true},
        { "value": "Using a custom Executor whose thread pool size matches the number of shops", "isCorrect": true},
        { "value": "None of these answers"}
      ],
      "explanation": "The preceding exercise provides a part of the answer, showing that we can already avoid the operations blocking one another by using a parallel stream. This condition is necessary. Besides, as long as the number of shops does not exceed the number of available threads, there will be no significant difference. Otherwise, we will need to increase the size of the thread pool using a custom Executor.",
      "hint": "As we already know, intermediate stream operations are lazy by nature, so they will be executed together when the final operation is triggered. Is there a way to avoid the blocking behaviour described on page 257, following Listing 11.11?"
    },
    {
      "index": 3,
      "type": "chooseOne",
      "text": "When writing non-blocking code, working with a custom Executor is more scalable than handling parallel streams.",
      "description": "Choose one",
      "options":
      [
        { "value": "True", "isCorrect": true},
        { "value": "False"}
      ],
      "hint": "Number of available processor cores...",
      "explanation": "A custom Executor can be set up with a user-defined number of threads that can be calculated according to the number of available processor cores, which can't be done using Collection.parallelStream()."
    }
  ],
  "questionGroup4":
  [
    {
      "index": 1,
      "type": "matchItemsToDescription",
      "text": "Match each CompletableFuture method to what it does.",
      "description": "Match items",
      "options":
      [
        { "value": "thenApply()"},
        { "value": "thenCombine()"},
        { "value": "thenCompose()"}
      ],
      "descriptions":
      [
        { "value": "Pipelines another asynchronous operation whose result will be used together with the result of this CompletableFuture as arguments of a given function.", "matchingIndex": 1},
        { "value": "Pipelines an asynchronous mapping function taking the result of a CompletableFuture as an argument and returning a new value.", "matchingIndex": 2},
        { "value": "Pipelines a mapping function taking the result of a CompletableFuture as an argument and returning a new value.", "matchingIndex": 0}
      ],
      "explanation": "The thenApply() and thenCompose() methods are related the same way as Stream's map() and flatMap() methods: thenCompose() takes a function that returns a CompletableFuture, allowing to pipeline asynchronous operations after one another. The thenCombine() method allows \"joining\" CompletableFutures into a function using their results to return some value.",
      "hint": "Look at the signature of each method. As an extra hint, if you look at the signature of some of these methods, you will see that they they take for argument a CompletionStage, which is a superinterface of CompletableFuture."
    },
    {
      "index": 2,
      "type": "chooseOne",
      "text": "To execute operations in an asynchronous way, using the *Async() versions (e.g., thenApplyAsync()) of CompletableFuture pipelining methods should be preferred to their standard counterparts.",
      "description": "Choose one",
      "options":
      [
        { "value": "True"},
        { "value": "False", "isCorrect": true}
      ],
      "hint": "What exactly is the difference between, for example, thenApply() and thenApplyAsync()?",
      "explanation": "The non-Async methods already properly manage asynchronous operations. All an *Async() method does is performing the pipeling process itself in a separate thread. Once the operation is pipelined, the remaining work is the same in both cases."
    }
  ],
  "questionGroup5":
  [
    {
      "index": 1,
      "type": "matchItemsToDescription",
      "text": "Match each CompletableFuture method to what it does.",
      "description": "Match items",
      "options":
      [
        { "value": "thenAccept()"},
        { "value": "thenAcceptBoth()"},
        { "value": "thenRun()"},
        { "value": "runAfterEither()"},
        { "value": "runAfterBoth()"}
      ],
      "descriptions":
      [
        { "value": "Pipelines an action taking as arguments the result of another CompletableFuture and the result of this one.", "matchingIndex": 1},
        { "value": "Executes an action after the first of this CompletableFuture and another one has completed.", "matchingIndex": 3},
        { "value": "Executes an action after both this CompletableFuture and another one have completed.", "matchingIndex": 4},
        { "value": "Pipelines an action taking no parameters and returning no result.", "matchingIndex": 2},
        { "value": "Pipelines an action taking the result of a CompletableFuture as an argument and returning no result.", "matchingIndex": 0}
      ],
      "explanation": "The thenAccept() and thenRun() methods are similar, both returning a CompletableFuture<Void>; the thenRun() method ignores any available result. So does  thenAcceptBoth(), which takes another argument in the form of a CompletableFuture (more exactly, its result). The names of the runAfter*() methods speak for themselves...",
      "hint": "Some of these methods are similar, having a version handling no-result actions and one handling functions."
    }
  ]
}
