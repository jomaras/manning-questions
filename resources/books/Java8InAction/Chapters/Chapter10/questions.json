{
  "questionGroup1":
  [
    {
      "index": 1,
      "type": "chooseOne",
      "text": "The null keyword does not represent anything. As a matter of fact, null instanceof Object returns false.",
      "description": "Choose one",
      "options":
      [
        { "value": "True", "isCorrect": true},
        { "value": "False"}
      ],
      "hint": "What is the semantic meaning of null?",
      "explanation": "As mentioned in section 10.1.2, null has no semantic meaning. A null pointer doesn't refer to anything and, as such, will make the instanceof operator return false whichever type is used as the second operand, even Object."
    }
  ],
  "questionGroup2":
  [
    {
      "index": 1,
      "type": "chooseMultiple",
      "text": "What are the necessary conditions to totally avoid NullPointerExceptions when using Optional?",
      "description": "Select all that apply",
      "options":
      [
        { "value": "Make sure all Optional references are non-null", "isCorrect": true},
        { "value": "Make sure Optional.ofNullable is used to convert any potentially null pointer to an Optional", "isCorrect": true},
        { "value": "Never invoke get() on an empty Optional"},
        { "value": "Turning all reference attributes into Optionals in a given class"}
      ],
      "explanation": "Basically, the only necessary condition is making sure no Optional references are left null. However, invoking Optional.of() with a null argument will throw a NullPointerException (NPE), so using the ofNullable() method on potentially null values can also be considered a necessary condition. Invoking get() on an empty Optional should also be avoided but it throws a NoSuchElementException, not a NPE. Using exclusively Optional-typed attributes in a class may be too much overhead if several attributes are considered mandatory (e.g., non-null fields in a class linked to a database table by an ORM layer). Besides, it doesn't eliminate the need to make sure no Optionals are left null and no null values are handled with Optional.of().",
      "hint": "What exactly is Optional as a Java data type? What is the behaviour of the methods available to build Optional instances?"
    }
  ],
  "questionGroup3":
  [
    {
      "index": 1,
      "type": "chooseOne",
      "text": "All Optional instances are immutable.",
      "description": "Choose one",
      "options":
      [
        { "value": "True", "isCorrect": true},
        { "value": "False"}
      ],
      "hint": "Look for setters in the Optional class methods...",
      "explanation": "The Optional class has no setters... Moreover, its value attribute is final."
    },
    {
      "index": 2,
      "type": "chooseOne",
      "text": "Invoking get() on an empty Optional returns null.",
      "description": "Choose one",
      "options":
      [
        { "value": "True"},
        { "value": "False", "isCorrect": true}
      ],
      "hint": "How is an empty Optional handled? Does it have any specific behaviour?",
      "explanation": "A NoSuchElementException is thrown. It's a simple enforcement of the goal of Optional objects, that is, avoiding handling null values."
    },
    {
      "index": 3,
      "type": "chooseMultiple",
      "text": "An operator that exists in some languages like the Spring Expression Language used to configure Spring beans is the Elvis Operator. It was also suggested as a new feature for Java 7 but was not selected for implementation. It works as shown below. What is a correct equivalent Optional-based form of String name = str ?: \"Undefined\";?",
      "description": "Select all that apply",
      "code": "String result = val ?: valueIfNull;\nis equivalent to\nString result = val != null ? val : valueIfNull;",
      "options":
      [
        { "value": "String name = Optional.of(str).ifNull(\"Undefined\");"},
        { "value": "String name = Optional.ofNullable(str).orElse(\"Undefined\");", "isCorrect": true},
        { "value": "String name = Optional.of(str).orElse(\"Undefined\");"},
        { "value": "String name = Optional.of(str == null ? \"Undefined\" : str);"}
      ],
      "hint": "What is the behaviour of the of() method? What does it return?",
      "explanation": "The third option is syntactically valid but throws a NullPointerException if str is null. The ofNullable() method allows passing a null value, which will result in an empty Optional. The orElse() method converts an Optional to a value of the encapsulated type while providing a default value in the case of an empty Optional. The last option is invalid because it returns an Optional<String>, not a String."
    },
    {
      "index": 4,
      "type": "matchItemsToDescription",
      "text": "Given the Person/Car/Insurance/name model, match each set of conditions to the corresponding combination of mapping operations.",
      "description": "Match items",
      "options":
      [
        { "value": "Car car, Insurance insurance, String name"},
        { "value": "Optional<Car> car, Insurance insurance, String name"},
        { "value": "Optional<Car> car, Optional<Insurance> insurance, String name"},
        { "value": "Car car, Optional<Insurance> insurance, Optional<String> name"}
      ],
      "descriptions":
      [
        { "value": "String name = Optional.of(person).flatMap(Person::getCar).map(Car::getInsurance).map(Insurance::getName).orElse(\"Unknown\");", "matchingIndex": 1},
        { "value": "String name = Optional.of(person).flatMap(Person::getCar).flatMap(Car::getInsurance).map(Insurance::getName).orElse(\"Unknown\");", "matchingIndex": 2},
        { "value": "String name = Optional.of(person).map(Person::getCar).map(Car::getInsurance).map(Insurance::getName).orElse(\"Unknown\");", "matchingIndex": 0},
        { "value": "String name = Optional.of(person).map(Person::getCar).flatMap(Car::getInsurance).flatMap(Insurance::getName).orElse(\"Unknown\");", "matchingIndex": 3}
      ],
      "explanation": "The logic is always the same: we use map() to map regular properties and flatMap() to map Optional properties, just like 'flatMapping' Stream properties when working with Streams.",
      "hint": "Be careful not to get an Optional<Optional<?>>..."
    },
    {
      "index": 5,
      "type": "chooseOne",
      "text": "If a type T implements Serializable, an Optional<T> is Serializable as well.",
      "description": "Choose one",
      "options":
      [
        { "value": "True"},
        { "value": "False", "isCorrect": true}
      ],
      "hint": "Which interfaces does Optional implement?",
      "explanation": "Optional is not a basic data type (e.g., for persistence purposes) and, as such, is not meant to be used as a serializable field type. This is why it does not implement Serializable."
    },
    {
      "index": 6,
      "type": "chooseOne",
      "text": "The orElseThrow() method allows throwing checked exceptions.",
      "description": "Choose one",
      "options":
      [
        { "value": "True", "isCorrect": true},
        { "value": "False"}
      ],
      "hint": "What types of argument does the orElseThrow() method accept?",
      "explanation": "The only restriction in the method signature is <X extends Throwable>, hence checked exception types are eligible."
    },
    {
      "index": 7,
      "type": "chooseOne",
      "text": "Filtering out a value in an Optional using filter() results into an empty Optional.",
      "description": "Choose one",
      "options":
      [
        { "value": "True", "isCorrect": true},
        { "value": "False"}
      ],
      "hint": "What does \"filtering out an optional value\" mean?",
      "explanation": "Filtering out a value from an Optional means leaving an empty placeholder, hence an empty Optional."
    },
    {
      "index": 8,
      "type": "chooseOne",
      "text": "What happens when filter() is applied to an empty Optional?",
      "description": "Choose one",
      "options":
      [
        { "value": "A NullPointerException is thrown."},
        { "value": "The supplied predicate is not applied and the empty Optional is returned unchanged.", "isCorrect": true},
        { "value": "A NoSuchElementException is thrown."},
        { "value": "The supplied predicate is applied with a null argument."}
      ],
      "hint": "What makes the most sense when it comes to an empty Optional?",
      "explanation": "As proven by the Optional source code, the supplied predicate is used only if the Optional is not empty. Otherwise, whichever the outcome of the predicate, which value should be actually returned since we don't have one at hand?"
    }
  ],
  "questionGroup4":
  [
    {
      "index": 1,
      "type": "chooseOne",
      "text": "The primitive Optional classes have the same mapping operations as their primitive Stream counterparts.",
      "description": "Choose one",
      "options":
      [
        { "value": "True"},
        { "value": "False", "isCorrect": true}
      ],
      "hint": "What are the methods defined in OptionalInt, OptionalLong, and OptionalDouble?",
      "explanation": "There are no mapping operations in these classes, which is why using them is way less interesting than using Int, Long, and DoubleStreams."
    }
  ]
}
