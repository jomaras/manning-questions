{
  "questionGroup1":
  [
    {
      "index": 1,
      "type": "chooseOne",
      "text": "When refactoring Java code, all functional interface implementations can be turned into lambda expressions.",
      "description": "Choose one",
      "options":
      [
        { "value": "True", "isCorrect": true},
        { "value": "False"}
      ],
      "hint": "Are there any blocking conditions? How can we manage them?",
      "explanation": "The two blocking conditions described in section 8.1.2 can be easily solved. A lambda expression can't shadow a variable from the enclosing class, but a variable renaming within the lambda can remove naming conflicts. If translating a functional interface implementation into a lambda raises a type ambiguity (as shown with the Runnable/Task example), it can be solved by casting the lambda to the proper type."
    },
    {
      "index": 2,
      "type": "chooseOne",
      "text": "When seeking brevity while refactoring code, all lambda expressions can and should be turned into method references.",
      "description": "Choose one",
      "options":
      [
        { "value": "True"},
        { "value": "False", "isCorrect": true}
      ],
      "hint": "Which lambda expressions can be turned into method references?",
      "explanation": "First of all, not all lambda expressions can be converted to method references. Besides, the code must remain clear and easily understandable after being refactored. The Dish::getCaloricLevel example shown in section 8.1.3 illustrates an ideal case where converting a lambda body into a \"referenced\" is straightforward (the method depends only on the Dish itself) and brings an added value (the code becomes easier to read and the getCaloricLevel() code becomes reusable)."
    },
    {
      "index": 3,
      "type": "chooseOne",
      "text": "Using lambdas for conditional deferred execution can help improve performance.",
      "description": "Choose one",
      "options":
      [
        { "value": "True", "isCorrect": true},
        { "value": "False"}
      ],
      "hint": "When is a lambda expression body executed?",
      "explanation": "As shown in section 8.1.5, a method that appears in a lambda expression body is executed only when the lambda itself is triggered. As am example, a lambda implementing Runnable won't be executed until the Runnable's run() method is explicitly invoked."
    }
  ],
  "questionGroup2":
  [
    {
      "index": 1,
      "type": "matchItemsToDescription",
      "text": "Match each JDK interface to the pattern it implements.",
      "description": "Match items",
      "options":
      [
        { "value": "java.io.FileFilter"},
        { "value": "java.awt.event.ActionListener"},
        { "value": "java.servlet.Filter"}
      ],
      "descriptions":
      [
        { "value": "Observer", "matchingIndex": 1},
        { "value": "Chain of responsibility", "matchingIndex": 2},
        { "value": "Strategy", "matchingIndex": 0}
      ],
      "explanation": "The File.listFiles() method accepts a FileFilter, whose accept() method shows illustrate how the Strategy pattern works. ActionListener is one of several listener/observer interfaces present in the AWT/Swing API. Through its doFilter() method that takes a FilterChain, the Filter interface is an example of chain of responsibility.",
      "hint": "Look at the Filter.doFilter() method."
    },
    {
      "index": 2,
      "type": "chooseOne",
      "text": "Which pattern do the Function default methods compose() and andThen() implement?",
      "description": "Choose one",
      "options":
      [
        { "value": "Strategy"},
        { "value": "Template method"},
        { "value": "Observer"},
        { "value": "Chain of responsibility", "isCorrect": true},
        { "value": "Factory"}
      ],
      "hint": "Study carefully the signature and the semantics of these two methods.",
      "explanation": "These two methods are used to chain functions. An example is shown in section 8.2.4, where two UnaryOperators are chained using the andThen() method."
    }
  ],
  "questionGroup3":
  [
    {
      "index": 1,
      "type": "matchItemsToDescription",
      "text": "Match each testing approach to its preferred technique.",
      "description": "Match items",
      "options":
      [
        { "value": "Testing the behaviour of a visible lambda"},
        { "value": "Focusing on the behaviour of a method using a lambda"},
        { "value": "Testing a complex lambda"},
        { "value": "Testing a higher-order function that takes a lambda as an argument"}
      ],
      "descriptions":
      [
        { "value": "Test the method with various lambdas", "matchingIndex": 3},
        { "value": "Write a unit test that checks the global behaviour of the calling method", "matchingIndex": 1},
        { "value": "Transfer the lambda implementation into a method and use a reference to this method", "matchingIndex": 2},
        { "value": "Calling the lambda like any other method", "matchingIndex": 0}
      ],
      "explanation": "The File.listFiles() method accepts a FileFilter, whose accept() method shows illustrate how the Strategy pattern works. ActionListener is one of several listener/observer interfaces present in the AWT/Swing API. Through its doFilter() method that takes a FilterChain, the Filter interface is an example of chain of responsibility.",
      "hint": "Look at the Filter.doFilter() method."
    }
  ],
  "questionGroup4":
  [
    {
      "index": 1,
      "type": "chooseOne",
      "text": "Lambda expressions can support breakpoints for debugging purposes.",
      "description": "Choose one",
      "options":
      [
        { "value": "True", "isCorrect": true},
        { "value": "False"}
      ],
      "hint": "What if the lambda spans several lines?",
      "explanation": "If a whole line of code is entirely part of a lambda (e.g., a multi-line block between braces), a breakpoint can be put on it. Be careful: if the breakpoint is put in the calling method but outside the lambda, the whole lambda code will be executed atomically when stepping in the code."
    },
    {
      "index": 2,
      "type": "chooseOne",
      "text": "What is the output of the following code?",
      "description": "Choose one",
      "code": "List<Integer> numbers = Arrays.asList(1, 2, 1, 3, 3, 2, 4);\nnumbers.stream()\n    .peek(n -> System.out.printf(\"before filter: %d%n\", n))\n    .filter(i -> i % 2 == 0)\n    .peek(n -> System.out.printf(\"after filter: %d%n\", n))\n    .distinct()\n    .peek(n -> System.out.printf(\"after distinct: %d%n\", n))\n    .forEach(System.out::println);",
      "options":
      [
        { "value": "before filter: 1\nbefore filter: 2\nafter filter: 2\nafter distinct: 2\n2\nbefore filter: 1\nbefore filter: 3\nbefore filter: 3\nbefore filter: 2\nafter filter: 2\nbefore filter: 4\nafter filter: 4\nafter distinct: 4\n4", "isCorrect": true},
        { "value": "before filter: 2\nafter filter: 2\nafter distinct: 2\n2\nbefore filter: 2\nbefore filter: 4\nafter filter: 4\nafter distinct: 4\n4"},
        { "value": "before filter: 1\nafter filter: 1\nafter distinct: 1\nbefore filter: 2\nafter filter: 2\nafter distinct: 2\n2\nbefore filter: 1\nafter filter: 1\nafter distinct: 1\nbefore filter: 3\nafter filter: 3\nafter distinct: 3\nbefore filter: 3\nafter filter: 3\nafter distinct: 3\nbefore filter: 2\nafter filter: 2\nafter distinct: 2\nbefore filter: 4\nafter filter: 4\nafter distinct: 4\n4"},
        { "value": "before filter: 1\nbefore filter: 2\nafter filter: 2\nafter distinct: 2\nbefore filter: 1\nbefore filter: 3\nbefore filter: 3\nbefore filter: 2\nafter filter: 2\nbefore filter: 4\nafter filter: 4\nafter distinct: 4\n2\n4"}
      ],
      "hint": "What does peek() actually peek at?",
      "explanation": "The peek() operation gives access to the current stream element and forwards it to the following operation without consuming it. Therefore, the first peek() forwards all elements, the second one receives only even integers, and the third one receives only the first occurrence of integers that passed the previous steps (i.e. even integers). No buffering is done when an element passes through the whole pipeline, hence the fourth option is invalid."
    }
  ]
}
