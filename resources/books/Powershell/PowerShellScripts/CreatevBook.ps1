﻿Import-Module "$PSScriptRoot\ManningvBook.psm1"

[xml]$Outline = gc 'C:\Dropbox\Side Business\PowerShell MOL vBook Exercises\outline.txt'
$Chapters = $outline.opml.body.outline.outline

## Build for all
foreach ($chapter in $Chapters)
{
	$chapternumber = $chapter.text.Split(' ')[1]
	new-vbookchapter -ChapterNumber $chapternumber
	$Questions = $chapter.outline
	foreach ($question in $Questions) {
		try
		{
			$questionType = $question.text.split('-')[1].Trim()
			$explanation = ($question.outline | where { $_.text -eq 'Explanation' })._note
			$temp = $question.text.Split('-')
			$text = ($temp[2..($temp.Length-1)] -join '-').Trim()
			if ($questionType -eq 'TrueFalse')
			{
				$questionType = 'Multiple Choice (Single)'	
			}
			
			if ($questionType -eq 'ArrangetheCode')
			{
				$answers = ($question.outline | where { $_.text -eq 'Answers' }).outline
				$options = ($answers | ? { $_.text -eq 'options' }).outline.text
				$ops = @()
				for ($i = 0; $i -lt $options.count; $i++)
				{
					$ops += New-vBookQuestionOption -QuestionType $questionType -CodeSnipIndex $i -Value $options[$i]
				}
				$template = ($answers | ? { $_.text -eq 'template' })._note
				$question = New-vBookQuestion -ChapterNumber $chapternumber -Option $ops -Explanation $explanation -Type $questiontype -Text $text -Template $template
				Add-vBookQuestion -Question $question -ChapterNumber $chapternumber
			}
			elseif ($questionType -eq 'MultipleItems')
			{
				$answers = ($question.outline | where { $_.text -eq 'Answers' }).outline
				$correct = ($answers | ? { $_.text -eq 'Correct' }).outline.text
				$incorrect = ($answers | ? { $_.text -eq 'Incorrect' }).outline.text
				$allanswers = $correct + $incorrect
				
				$options = @()
				
				for ($i=0; $i -lt $allanswers.Count; $i++) {
					$params = @{ 'Value' = $allanswers[$i] }
					if ($correct -contains $allanswers[$i])
					{
						$params.IsCorrect = $true
					}
					$options += New-vBookQuestionOption @params;
				}
				
				$question = New-vBookQuestion -ChapterNumber $chapternumber -Option $options -Explanation $explanation -Type $questiontype -Text $text
				Add-vBookQuestion -Question $question -ChapterNumber $chapternumber
			}
			elseif ($questionType -eq 'Multiple Choice (Single)')
			{
				$options = @();
				$i = 0;
				($question.outline | where { $_.text -eq 'Answers' }).outline | foreach {
					$params = @{ 'Value' = $_.text };
					if ($i -eq 0)
					{
						$params.IsCorrect = $true
					}
					$options += New-vBookQuestionOption @params;
					$i++
				}
				$question = New-vBookQuestion -ChapterNumber $chapternumber -Option $options -Explanation $explanation -Type $questiontype -Text $text
				Add-vBookQuestion -Question $question -ChapterNumber $chapternumber
			}
		}
		catch
		{
			$_
		}
	}
}



