﻿#Requires -Version 4

$BitBucketRepoPath = 'C:\Dropbox\Side Business\PowerShell MOL vBook Exercises\jomaras-manning-questions-142a750b6f3b'

$Defaults = @{
	'vBookName' = 'PowerShellMonthOfLunches'
	'ChapterContentFileName' = 'content.html'
	'ChapterQuestionsTemplate' = "$BitBucketRepoPath\resources\books\questions.json"
	'QuestionTypes' = 'Match to Description', 'Free Text', 'Arrange Code', 'Multiple Choice (Multiple)', 'Multiple Choice (Single)'
	'BookStructureTemplate' = "$BitBucketRepoPath\resources\books\MyTestBook\structure.json"
}

#region function New-vBook
function New-vBook
{
	[CmdletBinding()]
	param
	(
		[Parameter()]
		[ValidateNotNullOrEmpty()]
		[string]$Name = $Defaults.vBookName,
	
		[Parameter()]
		[ValidateNotNullOrEmpty()]
		[ValidateScript({ Test-Path -Path $_ -PathType Container })]
		[string]$FolderPath = "$BitBucketRepoPath\resources\books",
	
		[Parameter()]
		[ValidateNotNullOrEmpty()]
		[ValidateScript({ Test-Path -Path $_ -PathType Leaf })]
		[string]$StructureTemplatePath = $Defaults.BookStructureTemplate
		
	)
	begin
	{
		$vBookFolderPath = "$FolderPath\$($Name -replace ' ','')"
	}
	process {
		try
		{
			## Create the vBook folder
			if (-not (Test-Path -Path $vBookFolderPath -PathType Container))
			{
				$null = mkdir -Path $vBookFolderPath
				## Create the Chapters folder
				$null = mkdir -Path "$vBookFolderPath\Chapters"
			}
			
			## Copy the JSON template to the folder
			Copy-Item -Path $StructureTemplatePath -Destination $vBookFolderPath
			
			
		}
		catch
		{
			Write-Error $_.Exception.Message
		}
	}
}
#endregion function New-vBook

#region function New-vBookChapter
function New-vBookChapter
{
	[CmdletBinding()]
	param
	(
		
		[Parameter(Mandatory)]
		[ValidateNotNullOrEmpty()]
		[int]$ChapterNumber,
		
		[Parameter()]
		[ValidateNotNullOrEmpty()]
		[object[]]$Question,
		
		[Parameter()]
		[ValidateNotNullOrEmpty()]
		[string]$vBookName = $Defaults.vBookName,
		
		[Parameter()]
		[ValidateNotNullOrEmpty()]
		[ValidateScript({ Test-Path -Path ($_ | Split-Path -Parent) -PathType Container })]
		[string]$FolderPath = "$BitBucketRepoPath\resources\books\$vBookName\Chapters\Chapter$ChapterNumber"
	)
	process
	{
		try
		{
			
			## Create the chapter folder
			if (-not (Test-Path -Path $FolderPath -PathType Container))
			{
				$null = mkdir $FolderPath
			}
			
			## Add the content file here ##
			
			if ($PSBoundParameters.ContainsKey('Question')) {
				## Copy the question template file to the chapter folder
				Copy-Item -Path $Defaults.ChapterQuestionsTemplate -Destination $FolderPath
				
				## Add the JSON question to the questions template
				Add-vBookQuestion -ChapterNumber $ChapterNumber -Question $Question
				
			}
			
		}
		catch
		{
			Write-Error $_.Exception.Message
		}
	}
}
#endregion function New-vBookChapter

#region function Add-vBookContent
function Add-vBookContent
{
	[CmdletBinding()]
	param
	(
		
		[Parameter(Mandatory)]
		[ValidateNotNullOrEmpty()]
		[string]$ChapterName,
		
		[Parameter()]
		[ValidateNotNullOrEmpty()]
		[string]$vBookName = $Defaults.vBookName,
		
		[Parameter()]
		[ValidateNotNullOrEmpty()]
		[string]$ContentFilePath = "$BitBucketRepoPath\resources\books\$vBookName\Chapters\$ChapterName\$($Defaults.ChapterContentFileName)"
	)
	process
	{
		try
		{
			## Create a blank content HTML file
			if (-not (Test-Path -Path $ContentFilePath -PathType Leaf))
			{
				$null = New-Item -Path $ContentFilePath	-Type File
			}
		}
		catch
		{
			Write-Error $_.Exception.Message
		}
	}
}
#endregion function Add-vBookContent

#region function Add-vBookQuestion
function Add-vBookQuestion
{
	[CmdletBinding()]
	param
	(
		[Parameter(Mandatory)]
		[ValidateNotNullOrEmpty()]
		[object[]]$Question,
		
		[Parameter(Mandatory)]
		[ValidateNotNullOrEmpty()]
		[int]$ChapterNumber,
		
		[Parameter()]
		[ValidateNotNullOrEmpty()]
		[string]$QuestionsFilePath = "$BitBucketRepoPath\resources\books\$($Defaults.vBookName)\Chapters\Chapter$ChapterNumber\questions.json"
	)
	process
	{
		try
		{
			if (-not (Test-Path -Path $QuestionsFilePath -PathType Leaf))
			{
				Copy-Item -Path $Defaults.ChapterQuestionsTemplate -Destination ($QuestionsFilePath | Split-Path -Parent)
			}
			
			$questionJson = Get-Content -Path $QuestionsFilePath -Raw | ConvertFrom-Json
			$questionJson.questionGroup1 += $question
			$temp = $questionJson | ConvertTo-Json -Depth 3 | Set-Content -Path $QuestionsFilePath
		}
		catch
		{
			Write-Error $_.Exception.Message
		}
	}
}
#endregion function Add-vBookQuestion

#region function New-vBookQuestion
function New-vBookQuestion
{
	[CmdletBinding(DefaultParameterSetName = 'None')]
	param
	(
		
		[Parameter(Mandatory)]
		[ValidateNotNullOrEmpty()]
		[int]$ChapterNumber,
		
		[Parameter(Mandatory)]
		[ValidateNotNullOrEmpty()]
		[object[]]$Option,
		
		[Parameter(Mandatory)]
		[ValidateNotNullOrEmpty()]
		[string]$Explanation = '',
		
		[Parameter(Mandatory)]
		[ValidateNotNullOrEmpty()]
		[string]$Type,
		
		[Parameter()]
		[ValidateNotNullOrEmpty()]
		[string]$Hint = '',
		
		[Parameter()]
		[ValidateNotNullOrEmpty()]
		[string]$Text = '',
		
		[Parameter()]
		[ValidateNotNullOrEmpty()]
		[string]$QuestionDescription = '',
		
		[Parameter(Mandatory,ParameterSetName = 'MatchDescription')]
		[ValidateNotNullOrEmpty()]
		[object[]]$AnswerDescription,
	
		[Parameter(Mandatory,ParameterSetName = 'ArrangeTheCode')]
		[ValidateNotNullOrEmpty()]
		[string]$Template

	)
	process
	{
		try
		{
			
			$question = @{
				'Index' = Get-NextQuestionIndex -QuestionGroupName 'questionGroup1' -ChapterNumber $ChapterNumber
				'Hint' = $Hint
				'Explanation' = $Explanation
			}
			
			switch ($Type) {
				'Match to Description' {
					$question.Type = 'matchItemsToDescription'
					$question.Description = 'Match each item to the description'
					$question.Options = $Option
				}
				'Free Text' {
					$question.Type = 'giveAnswer'
					$question.Description = 'Fill in the blank'
					$question.Options = $Option
				}
				'ArrangetheCode' {
					$question.Type = 'arrangeCode'
					$question.Description = $Text
					$question.Text = 'Arrange the code.'
					
					for ($i=0; $i -lt $Option.Count; $i++) {
						$Template = $Template.Replace("($i)", "{ .$(($Option | ? { $_.CodeSnipIndex -eq $i }).value). }")
					}
					$question.Code = $Template.Replace("\n", '\n ')
				}
				'MultipleItems' {
					$question.Type = 'matchItemsToDescription'
					$question.Text = $Text
					$question.Options = $Option
				}
				'Multiple Choice (Single)' {
					$question.Type = 'chooseOne'
					$question.Text = $Text
					$question.Description = 'Choose one.'
					$question.Options = $Option
				}
				default {
					#<code>
				}
			}
			$question
		}
		catch
		{
			Write-Error $_.Exception.Message
		}
	}
}
#endregion function New-vBookQuestion

#region function New-vBookQuestionOption
function New-vBookQuestionOption
{
	[CmdletBinding(DefaultParameterSetName = 'None')]
	param
	(
		[Parameter()]
		[ValidateNotNullOrEmpty()]
		[string]$QuestionType,
		
		[Parameter(Mandatory,ParameterSetName = 'ArrangeTheCode')]
		[ValidateNotNullOrEmpty()]
		[int]$CodeSnipIndex,
		
		[Parameter(Mandatory)]
		[ValidateNotNullOrEmpty()]
		[string]$Value,
	
		[Parameter()]
		[ValidateNotNullOrEmpty()]
		[switch]$IsCorrect

	)
	process
	{
		try
		{
			$option = @{ 'value' = $Value }
			if ($PSCmdlet.ParameterSetName -eq 'ArrangeTheCode')
			{
				$option.CodeSnipIndex = $CodeSnipIndex
			}
			else
			{
				if ($IsCorrect.IsPresent)
				{
					$option.isCorrect = 'true'
				}
			}
			[pscustomobject]$option
		}
		catch
		{
			Write-Error $_.Exception.Message
		}
	}
}
#endregion function New-vBookQuestionOption

#region function New-vBookQuestionDescription
function New-vBookQuestionDescription
{
	[CmdletBinding()]
	param
	(
		[Parameter(Mandatory)]
		[ValidateNotNullOrEmpty()]
		[string]$Value,
		
		[Parameter(Mandatory)]
		[ValidateNotNullOrEmpty()]
		[int]$MatchingIndex
		
	)
	process
	{
		try
		{
			$option = @{
				'value' = $Value
				'matchingIndex' = $MatchingIndex	
			}
			[pscustomobject]$option
		}
		catch
		{
			Write-Error $_.Exception.Message
		}
	}
}
#endregion function New-vBookQuestionDescription

#region function Add-vBookQuestionGroup
function Add-vBookQuestionGroup
{
	[CmdletBinding()]
	param
	(
		
		[Parameter(Mandatory)]
		[ValidateNotNullOrEmpty()]
		[string]$QuestionGroupName,

		[Parameter(Mandatory)]
		[ValidateNotNullOrEmpty()]
		[string]$ChapterName,
		
		[Parameter()]
		[ValidateNotNullOrEmpty()]
		[string]$vBookName = $Defaults.vBookName,
		
		[Parameter()]
		[ValidateNotNullOrEmpty()]
		[ValidateScript({ Test-Path -Path $_ -PathType Leaf })]
		[string]$QuestionsFilePath = "$BitBucketRepoPath\resources\books\$vBookName\Chapters\$ChapterName\$($Defaults.ChapterQuestionsFileName)"
	)
	process
	{
		try
		{

		}
		catch
		{
			Write-Error $_.Exception.Message
		}
	}
}
#endregion function Add-vBookQuestionGroup

#region function Get-NextQuestionIndex
function Get-NextQuestionIndex
{
	[CmdletBinding()]
	param
	(
		
		[Parameter(Mandatory)]
		[ValidateNotNullOrEmpty()]
		[string]$QuestionGroupName,
	
		[Parameter(Mandatory)]
		[ValidateNotNullOrEmpty()]
		[int]$ChapterNumber,
		
		[Parameter()]
		[ValidateNotNullOrEmpty()]
		[string]$vBookName = $Defaults.vBookName,
		
		[Parameter()]
		[ValidateNotNullOrEmpty()]
		[ValidateScript({ Test-Path -Path $_ -PathType Leaf })]
		[string]$QuestionsFilePath = "$BitBucketRepoPath\resources\books\$vBookName\Chapters\Chapter$ChapterNumber\questions.json"
	)
	process
	{
		try
		{
			if (-not (Test-Path -Path $QuestionsFilePath -PathType Leaf))
			{
				Write-Verbose -Message "No questions file at [$($QuestionsFilePath)]. Index starts at 1"
				1
			}
			else
			{
				$jsonfile = Get-Content -Path $QuestionsFilePath -Raw | ConvertFrom-Json
				if ($jsonfile.$QuestionGroupName.index)
				{
					$arr = ($jsonfile.$QuestionGroupName).index
					$index = [int]$arr[-1] + 1
					Write-Verbose -Message "Questions file [$($QuestionsFilePath)] found. Index is [$($index)]"
					$index
				}
				else
				{
					1
				}
			}
		}
		catch
		{
			Write-Error $_.Exception.Message
		}
	}
}
#endregion function Get-NextQuestionIndex