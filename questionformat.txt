{
  "questionGroup1":
  [
    {
      "type": "matchItemsToDescription",
      "text": "Match items to description",
      "description": "Additional description",
      "options":
      [
        { "value": "Option1"},
        { "value": "Option2"},
        { "value": "Option3"}
      ],
      "descriptions":
      [
        { "value": "Description 1:", "matchingIndex": 0},
        { "value": "Description 2:", "matchingIndex": 2},
        { "value": "Description 3:", "matchingIndex": 1}
      ],
      "explanation": "Some explanation",
      "hint": "A hint"
    },
    {
      "type": "chooseOne",
      "description": "Description",
      "text": "Question text",
      "explanation": "Explanation",
      "hint": "Hint",
      "options":
      [
        { "value": "True", "isCorrect": true},
        { "value": "False"}
      ]
    },
    {
      "type": "chooseMultiple",
      "text": "Choose multiple items",
      "description": "Description",
      "options":
      [
        { "value": "Option 1", "isCorrect": true},
        { "value": "Option 2"},
        { "value": "Option 3", "isCorrect": true}
      ],
      "explanation": "Explanation",
      "hint": "Hint"
    },
    {
      "type": "chooseOne",
      "description": "Description - all questions can have code",
      "text": "Text",
      "code": "function foo () {\n  console.log(foo.name);\n}",
      "explanation": "Explanation",
      "hint": "Hint",
      "options":
      [
        { "value": "value1"},
        { "value": "value2", "isCorrect": true}
      ]
    }
  ]
}
