<?php
class DomHelper
{
  public static function getElementsByClassName($dom, $className)
  {
    $finder = new DomXPath($dom);
    return $finder->query("//*[contains(@class, '$className')]");
  }

  public static function getElementByClassName($dom, $className)
  {
    $elements = self::getElementsByClassName($dom, $className);

    return $elements->length > 0 ? $elements->item(0)
                                 : null;
  }

  public static function getNodeOuterHtml($node)
  {
    if($node == null) { return ""; }

    $dom = new DOMDocument('1.0');
    $clone = $dom->importNode($node->cloneNode(true), true);
    $dom->appendChild($clone);

    return $dom->saveHTML();
  }

  public static function getTextContent($node)
  {
    if($node == null) { return ""; }

    return $node->textContent;
  }
}
