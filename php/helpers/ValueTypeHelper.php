<?php

require 'php/external/mustache/src/Mustache/Autoloader.php';
Mustache_Autoloader::register();

class ValueTypeHelper
{
  public static function fillTemplate($template, $fillerMap)
  {
    $m = new Mustache_Engine;

    return $m->render($template, $fillerMap);
  }

  public static function escapeForFileName($raw)
  {
    return preg_replace('/[^A-Za-z0-9_\-]/', '_', $raw);
  }

  public static function stringStartsWithOneOf($haystack, $needles)
  {
    foreach($needles as $needle)
    {
      if(self::stringStartsWith($haystack, $needle)){ return true; }
    }

    return false;
  }

  public static function stringStartsWith($haystack, $needle)
  {
    return $needle === "" || strrpos($haystack, $needle, -strlen($haystack)) !== FALSE;
  }

  public static function stringEndsWith($haystack, $needle)
  {
    return $needle === "" || strpos($haystack, $needle, strlen($haystack) - strlen($needle)) !== FALSE;
  }

  public static function stringEndsWithOneOf($haystack, $needles)
  {
    foreach($needles as $needle)
    {
      if(self::stringEndsWith($haystack, $needle)) { return true; }
    }

    return false;
  }

  public static function getSubstringBefore($haystack, $needle)
  {
    $position = strrpos($haystack, $needle);

    if($position === FALSE) { return $haystack; }

    return substr($haystack, 0, $position);
  }

  public static function getSubstringAfter($haystack, $needle)
  {
    $position = strrpos($haystack, $needle);

    if($position === FALSE) { return ""; }

    return substr($haystack, $position + 1, strlen($haystack) - $position);
  }

  public static function containsRegExMatch($haystack, $regex)
  {
    return preg_match($regex, $haystack);
  }

  public static function stringContainsNumber($haystack)
  {
    return preg_match('/[\d]/', $haystack);
  }

  public static function replaceRegEx($regex, $replacement, $haystack)
  {
    return preg_replace($regex, $replacement, $haystack);
  }

  public static function containsString($haystack, $needle)
  {
    return strpos($haystack, $needle) !== false;
  }
}
