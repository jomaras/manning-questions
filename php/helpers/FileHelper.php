<?php
require_once("php/helpers/ValueTypeHelper.php");

class FileHelper
{
  public static function readFile($filePath)
  {
    return file_exists($filePath) ? file_get_contents($filePath)
                                  : "";
  }

  public static function readHtmlFile($filePath)
  {
    return mb_convert_encoding(file_get_contents($filePath), "HTML-ENTITIES", "UTF-8");
  }

  public static function writeToFile($filePath, $fileContent)
  {
    file_put_contents($filePath, $fileContent);
  }

  public static function createFolder($folderPath)
  {
    mkdir($folderPath);
  }

  public static function deleteFolderIfExists($folderPath)
  {
    if(!file_exists($folderPath)) { return; }

    if (!is_dir($folderPath)) { return unlink($folderPath); }

    foreach (scandir($folderPath) as $item)
    {
      if ($item == '.' || $item == '..') { continue; }

      if (!self::deleteFolderIfExists($folderPath . "/" . $item)) { return false; }
    }

    return rmdir($folderPath);
  }

  public static function copyFiles($sourceFolder, $destinationFolder, $extensions)
  {
    foreach(scandir($sourceFolder) as $item)
    {
      if($item == "." || $item == "..") { continue; }

      if(ValueTypeHelper::stringEndsWithOneOf($item, $extensions))
      {
        copy($sourceFolder . $item, $destinationFolder . $item);
      }
    }
  }

  public static function getFileName($filePath, $extension)
  {
    return basename($filePath, $extension);
  }

  public static function getDirectory($filePath)
  {
    $lastIndex = strrpos($filePath, "/");

    if($lastIndex === FALSE) { return "";}

    return substr($filePath, 0, $lastIndex);
  }

  public static function unzipFile($sourceFile, $destinationFolder)
  {
    $zip = new ZipArchive();

    $result = $zip->open($sourceFile);

    if ($result === TRUE)
    {
      $zip->extractTo($destinationFolder);
      $zip->close();
      return true;
    }
    else
    {
      return false;
    }
  }
}
