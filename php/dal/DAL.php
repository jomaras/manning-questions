<?php

require_once("php/helpers/FileHelper.php");
require_once("php/model/BookStructure.php");

class DAL
{
  private static $BOOKS_FOLDER = "resources/books/";

  public static function getCurrentChapterRootFolder($bookStructure)
  {
    return self::getBookDirectory() . "/Chapters/Chapter" . self::getChapterIndex($bookStructure) . "/";
  }

  public static function getCurrentChapterAudioFilePath($bookStructure)
  {
    return self::getCurrentChapterRootFolder($bookStructure) . "audio/chapter" . self::getChapterIndex($bookStructure) . ".mp3";
  }

  public static function getCurrentChapterSubtitleFilePath($bookStructure)
  {
    return self::getCurrentChapterRootFolder($bookStructure) . "audio/subtitles/chapter" . self::getChapterIndex($bookStructure) . ".vtt";
  }

  public static function getCurrentChapterQuestionsPath($bookStructure)
  {
    return self::getCurrentChapterRootFolder($bookStructure) . "questions.json";
  }

  public static function getChapterIndex($bookStructure)
  {
    $index = self::_getRequestKeyValue("chapter");

    $index = is_numeric($index) ? $index : 1;

    if($index > $bookStructure->getNumberOfChapters()
    || $index <= 0)
    {
      $index = 1;
    }

    return $index;
  }

  public static function getBookStructureFilePath()
  {
    return self::getBookDirectory() . "/structure.json";
  }

  public static function getBookContentFilePath($chapterIndex)
  {
    return self::getBookDirectory()
        . "/Chapters"
        . "/Chapter" . $chapterIndex
        . "/content.html";
  }

  public static function getBookDirectory()
  {
    return self::$BOOKS_FOLDER . self::getBookName();
  }

  public static function getBookName()
  {
    $bookName = urldecode(self::_getRequestKeyValue("book"));

    if($bookName === "") { return "ExampleBook"; }

    return $bookName;
  }

  public static function getBookStructureInParts($filePath)
  {
    $json = json_decode(FileHelper::readFile($filePath));

    if($json == null) { die("Invalid file content: " . $filePath); }
    if($json->parts == null) { die("Invalid JSON object:" . $filePath); }

    $parts = array();

    foreach($json->parts as $jsonPart)
    {
      $parts[] = self::_convertPart($jsonPart);
    }

    return new BookStructureInParts($parts);
  }

  private static function _convertPart($jsonPart)
  {
    $chapters = array();

    foreach($jsonPart->chapters as $jsonChapter)
    {
        $chapters[] = self::_convertChapter($jsonChapter);
    }

    return new BookPart($jsonPart->title, $jsonPart->index, $jsonPart->path, $chapters);
  }

  private static function _convertChapter($jsonChapter)
  {
    $sections = array();

    foreach($jsonChapter->sections as $jsonSection)
    {
      $sections[] = self::_convertSection($jsonSection);
    }

    return new BookChapter($jsonChapter->title, $jsonChapter->index, $jsonChapter->path, $sections);
  }

  private static function _convertSection($jsonSection)
  {
    return new BookChapterSection($jsonSection->title, $jsonSection->index, $jsonSection->path);
  }

  private static function _getRequestKeyValue($key)
  {
    return isset($_REQUEST[$key]) ? $_REQUEST[$key]
                                  : "";
  }
}
