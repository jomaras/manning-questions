<?php

require_once("php/dal/DAL.php");
require_once("php/helpers/FileHelper.php");

class DropdownGenerator
{
  public static function generateChaptersMarkup()
  {
    $bookStructureInParts = DAL::getBookStructureInParts(DAL::getBookStructureFilePath());

    $html = "<ul class='dropdown book-full'>";
    $chapterIndex = DAL::getChapterIndex($bookStructureInParts);

    foreach($bookStructureInParts->getParts() as $part)
    {
      $html .= "<li class='book-part'> Part " . $part->getTitle() . "</li>"
            . self::_generateChaptersDropdownMarkup($part->getChapters(), $chapterIndex);
    }

    $html .= "</ul>";

    return $html;
  }

  private static function _generateChaptersDropdownMarkup($chapters, $chapterIndex)
  {
    $html = "";

    foreach($chapters as $chapter)
    {
      $isOpenedClass = $chapter->getIndex() == $chapterIndex ? "opened contains-current-section" : "";
      $html .= "<li class='book-chapter has-dropdown $isOpenedClass'>"
            . self::_generateTocPositionPointHtml()
            . "<a class='chapter-link' href='" . self::_removeHash($chapter->getPath()) . "'>" . $chapter->getTitle() . "</a>"
            . self::_generateTocToggleButton()
            . self::_generateChaptersSectionsDropdownMarkup($chapter->getSections())
            . "</li>";
    }

    return $html;
  }

  private static function _generateChaptersSectionsDropdownMarkup($sections)
  {
    $html ="<ul class='dropdown book-sections-list'>";

    foreach($sections as $section)
    {
      $html .= "<li class='book-section'>"
              . self::_generateTocPositionPointHtml()
              . "<a href='" . $section->getPath() . "'>" . $section->getTitle() . "</a>"
            ."</li>";
    }

    $html .= "</ul>";

    return $html;
  }

  private static function _generateTocPositionPointHtml()
  {
    return "<div class='toc-position-point'><div class='toc-position-outer-point'><div class='toc-position-inner-point'><i></i></div></div></div>";
  }

  private static function _generateTocToggleButton()
  {
    return "<div class='toc-toggle-button'><i></i></div>";
  }

  private static function _removeHash($string)
  {
    $hashPosition = strpos($string, "#");

    if($hashPosition == false) { return $string; }

    return substr($string, 0, $hashPosition);
  }
}
