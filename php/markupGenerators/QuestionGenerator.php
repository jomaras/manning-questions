<?php
require_once("php/helpers/ValueTypeHelper.php");
require_once("php/helpers/FileHelper.php");

class QuestionGenerator
{
  private static $CHOOSE_MULTIPLE_TEMPLATE_PATH = "templates/quizes/chooseMultiple.html";
  private static $CHOOSE_ONE_TEMPLATE_PATH = "templates/quizes/chooseOne.html";
  private static $MATCH_ITEMS_TO_DESCRIPTION_TEMPLATE_PATH = "templates/quizes/matchItemsToDescription.html";
  private static $GIVE_ANSWER_TEMPLATE_PATH = "templates/quizes/giveAnswer.html";
  private static $ARRANGE_CODE_TEMPLATE_PATH = "templates/quizes/arrangeCode.html";

  private static $HEADER_TEMPLATE_PATH = "templates/quizes/header.html";
  private static $TOOLBAR_TEMPLATE_PATH = "templates/quizes/toolbar.html";
  private static $EXPLANATION_HINT_TEMPLATE_PATH = "templates/quizes/explanationHint.html";

  private static $CHOOSE_MULTIPLE_TEMPLATE = "";
  private static $CHOOSE_ONE_TEMPLATE = "";
  private static $MATCH_ITEMS_TO_DESCRIPTION_TEMPLATE = "";
  private static $GIVE_ANSWER_TEMPLATE = "";
  private static $ARRANGE_CODE_TEMPLATE = "";
  private static $TOOLBAR_TEMPLATE = "";
  private static $EXPLANATION_HINT_TEMPLATE = "";
  private static $HEADER_TEMPLATE = "";

  private static function getChooseMultipleTemplate()
  {
    if(self::$CHOOSE_MULTIPLE_TEMPLATE == "")
    {
      self::$CHOOSE_MULTIPLE_TEMPLATE = FileHelper::readFile(self::$CHOOSE_MULTIPLE_TEMPLATE_PATH);
    }

    return self::$CHOOSE_MULTIPLE_TEMPLATE;
  }

  private static function getChooseOneTemplate()
  {
    if(self::$CHOOSE_ONE_TEMPLATE == "")
    {
      self::$CHOOSE_ONE_TEMPLATE = FileHelper::readFile(self::$CHOOSE_ONE_TEMPLATE_PATH);
    }

    return self::$CHOOSE_ONE_TEMPLATE;
  }

  private static function getMatchItemsToDescriptionTemplate()
  {
    if(self::$MATCH_ITEMS_TO_DESCRIPTION_TEMPLATE == "")
    {
      self::$MATCH_ITEMS_TO_DESCRIPTION_TEMPLATE = FileHelper::readFile(self::$MATCH_ITEMS_TO_DESCRIPTION_TEMPLATE_PATH);
    }

    return self::$MATCH_ITEMS_TO_DESCRIPTION_TEMPLATE;
  }

  private static function getGiveAnswerTemplate()
    {
      if(self::$GIVE_ANSWER_TEMPLATE == "")
      {
        self::$GIVE_ANSWER_TEMPLATE = FileHelper::readFile(self::$GIVE_ANSWER_TEMPLATE_PATH);
      }

      return self::$GIVE_ANSWER_TEMPLATE;
    }

    private static function getArrangeCodeTemplate()
        {
          if(self::$ARRANGE_CODE_TEMPLATE == "")
          {
            self::$ARRANGE_CODE_TEMPLATE = FileHelper::readFile(self::$ARRANGE_CODE_TEMPLATE_PATH);
          }

          return self::$ARRANGE_CODE_TEMPLATE;
        }

  private static function getToolbarTemplate()
  {
    if(self::$TOOLBAR_TEMPLATE == "")
    {
      self::$TOOLBAR_TEMPLATE = FileHelper::readFile(self::$TOOLBAR_TEMPLATE_PATH);
    }

    return self::$TOOLBAR_TEMPLATE;
  }

  private static function getExplanationHintTemplate()
  {
    if(self::$EXPLANATION_HINT_TEMPLATE == "")
    {
      self::$EXPLANATION_HINT_TEMPLATE = FileHelper::readFile(self::$EXPLANATION_HINT_TEMPLATE_PATH);
    }

    return self::$EXPLANATION_HINT_TEMPLATE;
  }

  private static function getHeaderTemplate()
  {
    if(self::$HEADER_TEMPLATE == "")
    {
      self::$HEADER_TEMPLATE = FileHelper::readFile(self::$HEADER_TEMPLATE_PATH);
    }

    return self::$HEADER_TEMPLATE;
  }

  private static function getDescription($question)
  {
    if(isset($question["description"])) { return $question["description"]; }

    switch($question["type"])
    {
      case "chooseMultiple": return "Choose multiple";
      case "chooseOne": return "Choose one";
      case "matchItemsToDescription": return "Match items and description";
      case "giveAnswer": return "Give answer";
      case "giveAnswer": return "Arrange the Code";
      default: return "";
    }

    return "";
  }

  public static function generate($question)
  {
    $question["description"] = self::getDescription($question);

    $question["QUESTION_HEADER"] = ValueTypeHelper::fillTemplate(self::getHeaderTemplate(), $question);
    $question["TOOLBAR"] = ValueTypeHelper::fillTemplate(self::getToolbarTemplate(), $question);
    $question["EXPLANATION_HINT"] = ValueTypeHelper::fillTemplate(self::getExplanationHintTemplate(), $question);

    switch($question["type"])
    {
      case "chooseMultiple":
        return ValueTypeHelper::fillTemplate(self::getChooseMultipleTemplate(), $question);
      case "chooseOne":
        return ValueTypeHelper::fillTemplate(self::getChooseOneTemplate(), $question);
      case "matchItemsToDescription":
        return ValueTypeHelper::fillTemplate(self::getMatchItemsToDescriptionTemplate(), $question);
      case "giveAnswer":
        return ValueTypeHelper::fillTemplate(self::getGiveAnswerTemplate(), $question);
      case "arrangeCode":
        return ValueTypeHelper::fillTemplate(self::getArrangeCodeTemplate(), $question);
      default:
        return "";
    }
  }
}
