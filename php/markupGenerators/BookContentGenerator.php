<?php
require_once("php/helpers/ValueTypeHelper.php");
require_once("php/helpers/FileHelper.php");
require_once("php/markupGenerators/QuestionGenerator.php");
require_once("php/helpers/DomHelper.php");
require_once("php/dal/DAL.php");

class BookContentGenerator
{
  public static function getBookTitle()
  {
    return DAL::getBookName();
  }

  public static function generateMarkup()
  {
    $bookStructureInParts = DAL::getBookStructureInParts(DAL::getBookStructureFilePath());
    $chapterIndex = DAL::getChapterIndex($bookStructureInParts);

    return self::_fillQuizes
    (
      FileHelper::readFile(DAL::getBookContentFilePath($chapterIndex)),
      $bookStructureInParts,
      $chapterIndex
    );
  }

  public static function generateBookInfoMetadata()
  {
    $questionsPath = DAL::getCurrentChapterQuestionsPath(DAL::getBookStructureInParts(DAL::getBookStructureFilePath()));
    return "<meta id='book-info-meta' questionsPath='$questionsPath'>";
  }

  public static function generateChapterCompleteMarkup()
  {
    $bookStructureInParts = DAL::getBookStructureInParts(DAL::getBookStructureFilePath());

    $nextChapterIndex = DAL::getChapterIndex($bookStructureInParts) + 1;
    $nextChapter = $bookStructureInParts->getChapter($nextChapterIndex);

    if($nextChapter == null) { return ""; }

    if($nextChapterIndex >= $bookStructureInParts->getNumberOfChapters()) { return ""; }

    return ValueTypeHelper::fillTemplate
    (
      FileHelper::readFile("templates/endOfChapter.html"),
      array
      (
        "NEXT_CHAPTER_HEADING" => $nextChapter->getTitle(),
        "NEXT_CHAPTER_LOCATION" => $nextChapter->getPath(),
        "NEXT_CHAPTER_POINTS" => self::_getChapterIntroPoints(FileHelper::readFile(DAL::getBookContentFilePath($nextChapterIndex)))
      )
    );
  }

  public static function generateAudioMarkup()
  {
    $bookStructureInParts = DAL::getBookStructureInParts(DAL::getBookStructureFilePath());

    return ValueTypeHelper::fillTemplate
    (
        FileHelper::readFile("templates/audio.html"),
        array
        (
            "AUDIO_LOCATION" => DAL::getCurrentChapterAudioFilePath($bookStructureInParts),
            "SUBTITLE_LOCATION" => DAL::getCurrentChapterSubtitleFilePath($bookStructureInParts)
        )
    );
  }

  public static function getTemplatesAsScripts()
  {
    return self::_wrapInHtmlScript
    (
      "execution-environment-template",
      FileHelper::readFile("templates/executionEnvironment.html")
    );
  }

  private static function _wrapInHtmlScript($scriptId, $scriptContent)
  {
    return "<script type='x-template' id='$scriptId'>"
        .  $scriptContent
        . "</script>";
  }

  private static function _getChapterIntroPoints($chapterContent)
  {
    $dom = new DOMDocument();

    @$dom->loadHTML($chapterContent);

    return DomHelper::getNodeOuterHtml(DomHelper::getElementByClassName($dom, "introBulletList"));
  }

  private static function _fillQuizes($chapterMarkup, $bookStructureInParts, $chapterIndex)
  {
    $questionsJSON = FileHelper::readFile(DAL::getCurrentChapterQuestionsPath($bookStructureInParts));

    if(trim($questionsJSON) == "")
    {
      $questionsJSON = "{}";
    }

    $questions = json_decode($questionsJSON, $turnToAssociativeArray = true);

    $quizMap = array();

    if($questions != null)
    {
      $groupIndex = 0;

      foreach($questions as $quizId => $questionGroup)
      {
        $html = "";

        $questionIndex = 0;
        foreach($questionGroup as $question)
        {
          $question["index"] = $questionIndex + 1;

          if(array_key_exists("options", $question))
          {
            $optionIndex = 0;

            //Not sure what the problem is, but
            //if I put $option = $options[$optionIndex]
            //none of the modifications are saved outside of the loop
            for($optionIndex = 0; $optionIndex < count($question["options"]); $optionIndex++)
            {
              $question["options"][$optionIndex]["name"] = $groupIndex . "-" . $questionIndex;
              $question["options"][$optionIndex]["id"] = $question["options"][$optionIndex]["name"] . "-" . $optionIndex;
              $question["options"][$optionIndex]["index"] = $optionIndex;

              if(array_key_exists("matchingIndex", $question["options"][$optionIndex]))
              {
                $question["options"][$optionIndex]["matchingId"] = $question["options"][$optionIndex]["name"] . "-" . $question["options"][$optionIndex]["matchingIndex"];
              }

              $question["options"][$optionIndex]["correctStatus"] = array_key_exists("isCorrect", $question["options"][$optionIndex])
                                                                  ? "correct"
                                                                  : "incorrect";
            }

          }

          $html .= QuestionGenerator::generate($question);

          $questionIndex++;
        }

        $quizMap[$groupIndex++] = $html;
      }
    }

    $knowledgeQuizTemplate = FileHelper::readFile("templates/quizes/quizTemplate.html");

    return preg_replace_callback
    (
        '(\{\{\{EXERCISE\}\}\})',
        function ($matches) use (&$quizMap, $knowledgeQuizTemplate)
        {
            $questions = sizeof($quizMap) > 0 ? array_shift($quizMap) : null;
            return ValueTypeHelper::fillTemplate
            (
              $knowledgeQuizTemplate,
              array("QUESTIONS"=> $questions)
            );
        },
        $chapterMarkup
    );
  }
}
