<?php

class BookStructureInParts
{
  public function __construct($parts)
  {
      $this->parts = $parts;
      $this->numberOfChapters = 0;

      foreach($parts as $part)
      {
        $this->numberOfChapters += sizeof($part->getChapters());
      }
  }

  private $parts;
  public function getParts()
  {
    return $this->parts;
  }

  private $numberOfChapters;
  public function getNumberOfChapters()
  {
      return $this->numberOfChapters;
  }

  public function getChapter($chapterIndex)
  {
    foreach($this->parts as $part)
    {
      $chapter = $part->getChapter($chapterIndex);

      if($chapter != null)
      {
        return $chapter;
      }
    }

    return $chapter;
  }
}

class BookStructureInChapters
{
  public function __construct($chapters)
  {
    $this->chapters = $chapters;
  }

  private $chapters;
  public function getChapters()
  {
    return $this->chapters;
  }

  public function getChapter($chapterIndex)
  {
    foreach($this->chapters as $chapter)
    {
      if($chapter->getIndex() == $chapterIndex)
      {
        return $chapter;
      }
    }

    return null;
  }
}

class BookPart extends BookItem
{
  public function __construct($title, $index, $path, $chapters)
  {
    parent::__construct($title, $index, $path);
    $this->chapters = $chapters;
  }

  private $chapters;
  public function getChapters()
  {
      return $this->chapters;
  }

  public function getChapter($chapterIndex)
  {
    foreach($this->chapters as $chapter)
    {
      if($chapter->getIndex() == $chapterIndex)
      {
        return $chapter;
      }
    }

    return null;
  }
}

class BookChapter extends BookItem
{
  public function __construct($title, $index, $path, $sections)
  {
    parent::__construct($title, $index, $path);

    $this->sections = $sections;
  }

  private $sections;
  public function getSections()
  {
    return $this->sections;
  }
}

class BookChapterSection extends BookItem
{
  public function __construct($title, $index, $path)
  {
    parent::__construct($title, $index, $path);
  }
}

abstract class BookItem
{
  public function __construct($title, $index, $path)
  {
    $this->title = $title;
    $this->index = $index;
    $this->path = $path;
  }

  private $title;
  private $index;
  private $path;

  public function getTitle() { return $this->title; }
  public function getIndex() { return $this->index; }
  public function getPath() { return $this->path; }
}
