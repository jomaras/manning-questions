﻿<?php
ini_set('display_errors', 1);
require_once("php/markupGenerators/DropdownGenerator.php");
require_once("php/markupGenerators/BookContentGenerator.php");
?>
<!doctype html>
<html class="no-js" lang="en">
<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>Manning vBook</title>
  <?php echo(BookContentGenerator::generateBookInfoMetadata()); ?>
  <link rel="stylesheet" href="stylesheets/app.css" />
  <link rel="stylesheet" href="stylesheets/quiz.css" />
  <link rel="stylesheet" href="bower_components/codemirror/lib/codemirror.css"/>
  <script src="bower_components/modernizr/modernizr.js"></script>
</head>
<body>
  <div class="fixed">
    <nav class="top-bar" id="top-bar" data-topbar role="navigation">
      <section class="top-bar-section">
        <ul class="left">

          <li>
            <a href="#">
              <i class="icon-i-manning-m"></i>
            </a>
          </li>

          <li class="toc has-dropdown">
            <a href="#" id="toc-menu-button">
              <i class="icon-i-toc"></i>
            </a>
          </li>
        </ul>
      </section>

      <h1 class="hook-book-title book-title show-for-medium-up"><a href=""><?php echo(BookContentGenerator::getBookTitle())?></a></h1>
    </nav>
  </div>

  <div id="menu-toc" class="menu-toc content">
    <?php echo(DropdownGenerator::generateChaptersMarkup()); ?>
  </div>

  <div class="row" id="main-row">
    <div class="large-12 columns">

      <div class="text-wrapper">
        <div id="mainContentContainer">
          <div id="trackPointColumn">
          </div>

          <div id='bookMarkupContainer'>
            <?php echo(BookContentGenerator::generateMarkup()); ?>
          </div>
        </div> <!-- #mainContentContainer -->

      </div><!-- /.text-wrapper -->

    </div><!-- /.columns -->
  </div><!-- /.row -->

  <div id="modal-edit-code" class="reveal-modal" data-reveal>
    <a class="close-reveal-modal">×</a>
    <div class="model-edit-code-container">
      <textarea class="model-edit-code-text"></textarea>
    </div>
    <a href="#" class="button tiny button-reset right">Reset</a>
    <a href="#" class="button tiny button-run right">Run</a>
    <br class="clearfix">
    <label>Result:</label>
    <iframe id="iframe-edit-preview"></iframe>
  </div>

  <footer class="row">
    <div class="large-12 columns">
      <hr>
      <div class="row">
        <div class="large-6 columns">
          <p>&copy; Copyright 2015 Manning Publications</p>
        </div>
      </div>
    </div>
  </footer>

  <audio id="audioCorrectPlayer" src="resources/sounds/correct.mp3" preload="auto"></audio>
  <audio id="audioErrorPlayer" src="resources/sounds/error.mp3" preload="auto"></audio>

  <script type="text/javascript" src="js/prettify/prettify.js"></script>
  <script src="bower_components/jquery/dist/jquery.min.js"></script>
  <script src="bower_components/foundation/js/foundation.min.js"></script>
  <script src="bower_components/velocity/velocity.js"></script>
  <script src="bower_components/codemirror/lib/codemirror.js"></script>
  <script src="bower_components/codemirror/mode/htmlmixed/htmlmixed.js"></script>
  <script src="bower_components/codemirror/mode/xml/xml.js"></script>
  <script src="bower_components/codemirror/mode/javascript/javascript.js"></script>
  <script src="bower_components/codemirror/mode/css/css.js"></script>
  <script src="bower_components/jquery-ui/jquery-ui.min.js"></script>
  <script src="bower_components/mustache/mustache.min.js"></script>
  <script src="js/utils/jquery.touch.punch.js"></script>
  <script src="js/vBook/runnableScripts.js"></script>
  <script src="js/app.js"></script>
  <script src="js/vBook/toolbar/tocMenu.js"></script>
  <script src="js/vBook/quizes/quizes.js"></script>
  <script>
    $(document).ready(function()
    {
      prettyPrint();
    });
  </script>
</body>
</html>
